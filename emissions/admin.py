from django.contrib import admin

from .models import (
    Category,
    Emission,
    Episode,
    Focus,
    Format,
    NewsCategory,
    NewsItem,
    Nonstop,
    Schedule,
    SoundFile,
)


@admin.register(Emission)
class EmissionAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Episode)
class EpisodeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    pass


@admin.register(NewsCategory)
class NewsCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(NewsItem)
class NewsItemAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(SoundFile)
class SoundFileAdmin(admin.ModelAdmin):
    pass


@admin.register(Nonstop)
class NonstopAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Focus)
class FocusAdmin(admin.ModelAdmin):
    pass


@admin.register(Format)
class FormatAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
