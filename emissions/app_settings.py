from django.conf import settings


class AppSettings:
    def get_setting(self, setting, default=None):
        return getattr(settings, 'EMISSIONS_' + setting, default)

    @property
    def DAY_HOUR_START(self):
        return self.get_setting('DAY_HOUR_START', 4)

    @property
    def DAY_MINUTE_START(self):
        return self.get_setting('DAY_MINUTE_START', 30)

    @property
    def MAX_EPISODE_RUNNING_TIME(self):
        # maximum running time for an episode (in hours, default: 8)
        return self.get_setting('MAX_EPISODE_RUNNING_TIME', 8)

    @property
    def IMAGE_USAGE_TEXT(self):
        # custom help text for image usage field, to note about copyright and stuff
        # if left empty the checkbox will not be displayed.
        return self.get_setting('IMAGE_USAGE_TEXT', '')

    @property
    def USE_INFO_ONLY_SCHEDULE_FIELD(self):
        return self.get_setting('USE_INFO_ONLY_SCHEDULE_FIELD', False)

    @property
    def ABSENCE_NOTIFICATION_EMAILS(self):
        # list of addresses to notify when an absence is added
        return self.get_setting('ABSENCE_NOTIFICATION_EMAILS', [])


app_settings = AppSettings()
