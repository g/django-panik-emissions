import datetime
import os
import re
import unicodedata
import urllib
import uuid

import requests
from bs4 import BeautifulSoup
from django import forms
from django.conf import settings
from django.core.files.storage import DefaultStorage
from django.forms import ValidationError, fields
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_str
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from taggit.forms import TagWidget

from .app_settings import app_settings
from .models import (
    Absence,
    Category,
    Diffusion,
    Emission,
    Episode,
    NewsCategory,
    NewsItem,
    Picture,
    PlaylistElement,
    Schedule,
    SoundFile,
)
from .utils import get_duration
from .widgets import DateWidget, SplitDateTimeField, SplitDateTimeWidget


class DayAndHourWidget(forms.MultiWidget):
    def __init__(self, attrs=None):
        WEEKDAYS = [
            _('Monday'),
            _('Tuesday'),
            _('Wednesday'),
            _('Thursday'),
            _('Friday'),
            _('Saturday'),
            _('Sunday'),
        ]
        widgets = (
            forms.Select(attrs=attrs, choices=([(weekday, WEEKDAYS[weekday]) for weekday in range(7)])),
            forms.Select(attrs=attrs, choices=([(hour, hour) for hour in range(24)])),
            forms.Select(attrs=attrs, choices=([(minute, str(minute).zfill(2)) for minute in range(60)])),
        )
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.weekday(), value.hour, value.minute]
        return [None, None, None]

    def value_from_datadict(self, data, files, name):
        # we only care about day/hour/minutes, but we conveniently use a
        # datetime value to store that; we pick 2007 as reference year as
        # it had its January 1st on a Monday.
        data_list = [
            widget.value_from_datadict(data, files, name + '_%s' % i) for i, widget in enumerate(self.widgets)
        ]

        if data_list:
            return datetime.datetime(2007, 1, int(data_list[0]) + 1, int(data_list[1]), int(data_list[2]))
        return None


class JqueryFileUploadFileInput(forms.FileInput):
    template_name = 'emissions/upload.html'

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget'].update(
            {
                'upload_url': self.url,
                'files': self.files,
                'max_size': getattr(settings, 'SOUND_UPLOAD_MAX_SIZE', 2**30),  # 1 GB.
            }
        )
        return context

    def render(self, name, value, attrs=None):
        output = render_to_string(
            'emissions/upload.html',
            {
                'widget': {
                    'upload_url': self.url,
                    'files': self.files,
                    'name': name,
                }
            },
        )
        return mark_safe(output)


class JqueryFileUploadInput(forms.MultiWidget):
    needs_multipart_form = True
    upload_id_re = re.compile(r'^[a-z0-9A-Z-]+$')
    upload_id = None

    def __init__(self, attrs=None, choices=[], max_filename_length=None):
        self.max_filename_length = max_filename_length
        widget_list = (forms.HiddenInput(attrs=attrs), JqueryFileUploadFileInput(attrs=attrs))
        super().__init__(widget_list, attrs)

    def decompress(self, value):
        # map python value to widget contents
        if self.upload_id:
            pass
        elif isinstance(value, (list, tuple)) and value and value[0] is not None:
            self.upload_id = str(value[0])
        else:
            self.upload_id = str(uuid.uuid4())
        return [self.upload_id, None]

    def get_files_for_id(self, upload_id):
        storage = DefaultStorage()
        path = os.path.join('upload', upload_id)
        if not storage.exists(path):
            return
        for filepath in storage.listdir(path)[1]:
            name = os.path.basename(filepath)
            yield storage.open(os.path.join(path, name))

    def value_from_datadict(self, data, files, name):
        """
        If some file was submitted, that's the value,
        If a regular hidden_id is present, use it to find uploaded files,
        otherwise return an empty list
        """
        upload_id, file_input = super().value_from_datadict(data, files, name)
        if file_input:
            pass
        elif JqueryFileUploadInput.upload_id_re.match(upload_id):
            file_input = list(self.get_files_for_id(upload_id))
        else:
            file_input = []
        try:
            return file_input[0]
        except IndexError:
            return None

    def render(self, name, value, attrs=None, renderer=None):
        self.decompress(value)
        self.widgets[1].url = '/upload/%s/' % self.upload_id
        self.widgets[1].url = reverse('upload', kwargs={'transaction_id': self.upload_id})
        if self.max_filename_length:
            self.widgets[1].url += '?max_filename_length=%d' % self.max_filename_length
        self.widgets[1].files = '/upload/%s/' % self.get_files_for_id(self.upload_id)
        output = super().render(name, value, attrs)
        return output


class EmissionForm(forms.ModelForm):
    class Meta:
        model = Emission
        exclude = ('slug', 'has_focus', 'got_focus', 'chat_open', 'podcast_sound_quality')
        widgets = {'website': forms.Textarea(attrs={'rows': 2})}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not Category.objects.exists():
            del self.fields['categories']
        if app_settings.IMAGE_USAGE_TEXT:
            self.fields['image_usage_ok'].help_text = app_settings.IMAGE_USAGE_TEXT
        else:
            del self.fields['image_usage_ok']


class EpisodeForm(forms.ModelForm):
    class Meta:
        model = Episode
        exclude = (
            'slug',
            'has_focus',
            'got_focus',
            'effective_start',
            'effective_end',
            'first_diffusion_date',
        )
        widgets = {
            'emission': forms.HiddenInput(),
            'tags': TagWidget(),
            'extra_links': forms.Textarea(attrs={'rows': 2}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if settings.USE_AGENDA_ONLY_FIELD:
            self.fields['text'].required = False
            if self.instance and self.instance.agenda_only:
                self.instance.slug = None
        else:
            del self.fields['agenda_only']
        if app_settings.IMAGE_USAGE_TEXT:
            self.fields['image_usage_ok'].help_text = app_settings.IMAGE_USAGE_TEXT
        else:
            del self.fields['image_usage_ok']

    def clean(self):
        if settings.USE_AGENDA_ONLY_FIELD and not self.cleaned_data['agenda_only']:
            if not self.cleaned_data['text']:
                self.add_error('text', _('Missing description'))
        return self.cleaned_data


class EpisodeNewForm(EpisodeForm):
    first_diffusion = SplitDateTimeField(label=_('First Diffusion'), widget=SplitDateTimeWidget)
    second_diffusion = SplitDateTimeField(label=_('Second Diffusion'), widget=SplitDateTimeWidget)
    third_diffusion = SplitDateTimeField(label=_('Third Diffusion'), widget=SplitDateTimeWidget)
    fourth_diffusion = SplitDateTimeField(label=_('Fourth Diffusion'), widget=SplitDateTimeWidget)

    def get_diffusion_fields(self, emission):
        if emission:
            schedules = list(Schedule.objects.filter(emission=emission).order_by('datetime'))
            if len(schedules) > 1 and (
                schedules[0].weeks == schedules[1].weeks
                and schedules[0].datetime.date() == schedules[1].datetime.date()
            ):
                # special case for daily program with same-day rerun
                schedules = 2
            else:
                schedules = len([x for x in schedules if x.rerun]) + 1
        else:
            schedules = 1
        fields = ['first_diffusion']
        if schedules > 1:
            fields.append('second_diffusion')
        if schedules > 2:
            fields.append('third_diffusion')
        if schedules > 3:
            fields.append('fourth_diffusion')
        return fields

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        emission = kwargs.get('initial', {}).get('emission')
        free_datetime = bool(
            (user and user.has_perm('emissions.add_diffusion')) or not emission.schedule_set.exists()
        )
        diffusion_fields = self.get_diffusion_fields(emission)
        for field_name in list(self.fields.keys()):
            if field_name.endswith('_diffusion'):
                if field_name not in diffusion_fields:
                    # remove extra diffusion fields
                    del self.fields[field_name]
                elif not free_datetime:
                    # turn diffusion fields in select widgets for users
                    self.fields[field_name].widget = DiffusionDateTimeWidget(emission)

    def save(self, commit=True):
        episode = super().save(commit=commit)
        schedules = list(Schedule.objects.filter(emission=episode.emission).order_by('datetime'))
        for field_name in self.get_diffusion_fields(episode.emission):
            diffusion = Diffusion()
            diffusion.episode_id = episode.id
            diffusion.datetime = self.cleaned_data.get(field_name)
            if schedules and schedules[0].info_only_schedule:
                diffusion.info_only_schedule = True
            diffusion.save()
        return episode


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        exclude = ()
        widgets = {
            'emission': forms.HiddenInput(),
            'datetime': DayAndHourWidget(),
            'start_date': DateWidget(),
            'end_date': DateWidget(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not app_settings.USE_INFO_ONLY_SCHEDULE_FIELD:
            del self.fields['info_only_schedule']


class SoundFileForm(forms.ModelForm):
    class Meta:
        model = SoundFile
        exclude = ('has_focus', 'got_focus', 'duration', 'download_count', 'mp3_file_size', 'order')
        widgets = {
            'episode': forms.HiddenInput(),
            'file': JqueryFileUploadInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if getattr(settings, 'USE_EXTERNAL_SOUNDS', True):
            self.fields['file'].widget = forms.HiddenInput()
        else:
            self.fields['external_url'].widget = forms.HiddenInput()
        episode = kwargs.get('initial').get('episode')
        if episode.first_diffusion_date and episode.first_diffusion_date < datetime.datetime.now():
            self.fields['wait_after_diffusion'].widget = forms.HiddenInput()
        if len(list(self.fields['format'].widget.choices)) == 1:
            # empty entry
            self.fields['format'].widget = forms.HiddenInput()

    def clean_external_url(self):
        value = self.cleaned_data.get('external_url')
        if value and urllib.parse.urlparse(value).netloc == 'soundcloud.com':
            resp = requests.get('https://soundcloud.com/oembed', params={'format': 'json', 'url': value})
            try:
                resp.raise_for_status()
                embed_iframe_src = BeautifulSoup(resp.json()['html']).find('iframe').attrs['src']
                api_url = urllib.parse.parse_qs(urllib.parse.urlparse(embed_iframe_src).query)['url'][0]
            except Exception as e:
                raise ValidationError(_('Error getting soundcloud embedding infos (%s)') % e)
            return api_url
        return value

    def clean(self):
        super().clean()
        if getattr(settings, 'USE_EXTERNAL_SOUNDS', True):
            if not self.cleaned_data.get('external_url'):
                raise ValidationError(_('Missing URL'))
        else:
            if not self.cleaned_data.get('file'):
                raise ValidationError(_('Missing file'))
            if self.cleaned_data.get('file'):
                duration = get_duration(self.cleaned_data['file'].file.name)
                if not duration:
                    raise ValidationError(_('Invalid file, could not get duration.'))


class SoundFileEditForm(forms.ModelForm):
    class Meta:
        model = SoundFile
        exclude = (
            'has_focus',
            'got_focus',
            'file',
            'external_url',
            'duration',
            'download_count',
            'mp3_file_size',
        )
        widgets = {
            'episode': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if len(list(self.fields['format'].widget.choices)) == 1:
            # empty entry
            self.fields['format'].widget = forms.HiddenInput()
        self.fields = {
            key: self.fields[key]
            for key in ['title', 'format', 'podcastable', 'wait_after_diffusion', 'fragment', 'license']
        }
        if (
            self.instance.episode.first_diffusion_date
            and self.instance.episode.first_diffusion_date < datetime.datetime.now()
        ):
            self.fields['wait_after_diffusion'].widget = forms.HiddenInput()


class DiffusionDateTimeWidget(forms.Select):
    def __init__(self, emission, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.choices = list(self.options(emission))

    def options(self, emission):
        dates = []
        max_date = datetime.datetime.now() + datetime.timedelta(days=60)
        while True:
            if dates:
                since = dates[-1] + datetime.timedelta(minutes=10)
            else:
                since = datetime.datetime.now() - datetime.timedelta(days=30)
            dates.append(emission.get_next_planned_date(since, include_rerun=True))
            if dates[-1] > max_date:
                dates = dates[:-1]
                break
        for date in dates:
            yield (date, date.strftime('%d/%m/%Y %H:%M'))

    def value_from_datadict(self, data, files, name):
        return data[name].split()


class DiffusionForm(forms.ModelForm):
    class Meta:
        model = Diffusion
        exclude = ()
        field_classes = {
            'datetime': SplitDateTimeField,
        }
        widgets = {
            'episode': forms.HiddenInput(),
            'datetime': SplitDateTimeWidget(),
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        episode = kwargs.get('initial', {}).get('episode')
        emission = episode.emission
        if not app_settings.USE_INFO_ONLY_SCHEDULE_FIELD:
            del self.fields['info_only_schedule']
        if (user and user.has_perm('emissions.add_diffusion')) or not emission.schedule_set.exists():
            date = emission.get_next_planned_date()
            self.fields['datetime'].initial = date
            self.fields['datetime'].widget = SplitDateTimeWidget()
        else:
            self.fields['datetime'].widget = DiffusionDateTimeWidget(emission)


class NewsItemForm(forms.ModelForm):
    class Meta:
        model = NewsItem
        exclude = ('slug', 'has_focus', 'got_focus')
        widgets = {
            'tags': TagWidget(),
            'date': DateWidget(),
            'expiration_date': DateWidget(),
            'event_date': DateWidget(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user = kwargs.get('initial', {}).get('user')
        if not user.is_staff:
            self.fields['emission'].widget = forms.HiddenInput()
        if not NewsCategory.objects.exists():
            del self.fields['category']
        if app_settings.IMAGE_USAGE_TEXT:
            self.fields['image_usage_ok'].help_text = app_settings.IMAGE_USAGE_TEXT
        else:
            del self.fields['image_usage_ok']


class AbsenceDateTimeWidget(forms.Select):
    def __init__(self, emission, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.choices = list(self.options(emission))

    def options(self, emission):
        dates = []
        for i in range(4):
            if dates:
                since = dates[-1] + datetime.timedelta(minutes=10)
            else:
                since = None
            dates.append(emission.get_next_planned_date(since))
        for date in dates:
            yield (date, date.strftime('%d/%m/%Y %H:%M'))

    def value_from_datadict(self, data, files, name):
        return data[name].split()


class AbsenceForm(forms.ModelForm):
    class Meta:
        model = Absence
        exclude = ()
        field_classes = {
            'datetime': SplitDateTimeField,
        }
        widgets = {
            'emission': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        emission = kwargs.get('initial', {}).get('emission')
        user = kwargs.get('initial', {}).get('user')
        self.fields['datetime'].widget.emission = emission
        if user and user.has_perm('emissions.delete_diffusion'):
            date = emission.get_next_planned_date()
            self.fields['datetime'].initial = date
            self.fields['datetime'].widget = SplitDateTimeWidget()
        else:
            self.fields['datetime'].widget = AbsenceDateTimeWidget(emission)

    def save(self, commit=True):
        t = super().save(commit=commit)
        for i, schedule in enumerate(
            Schedule.objects.filter(emission=self.instance.emission, rerun=True).order_by('datetime')
        ):
            rerun_date = schedule.get_next_planned_date(self.instance.datetime)
            rerun_absence = Absence()
            rerun_absence.emission = self.instance.emission
            rerun_absence.datetime = rerun_date
            rerun_absence.save()
        return t


class PlaylistElementForm(forms.ModelForm):
    class Meta:
        model = PlaylistElement
        exclude = ('title', 'notes', 'order')
        widgets = {
            'episode': forms.HiddenInput(),
            'sound': JqueryFileUploadInput(),
        }


class PictureForm(forms.ModelForm):
    class Meta:
        model = Picture
        fields = (
            'image',
            'image_attribution_url',
            'image_attribution_text',
            'title',
            'alt_text',
            'episode',
            'emission',
            'newsitem',
        )
        widgets = {
            'episode': forms.HiddenInput(),
            'emission': forms.HiddenInput(),
            'newsitem': forms.HiddenInput(),
        }
