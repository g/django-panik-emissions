import datetime

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from ...models import Emission


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--emission', dest='emission_slug', metavar='SLUG', default=None)

    def handle(self, emission_slug, verbosity, **options):
        self.verbose = verbosity > 1
        emissions = Emission.objects.all()
        if emission_slug:
            emissions = emissions.filter(slug=emission_slug)
        emissions = emissions.filter(chat_open__lt=timezone.now() - datetime.timedelta(hours=24))
        for emission in emissions:
            emission.chat_open = None
            emission.save()
