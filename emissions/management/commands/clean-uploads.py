import os
import time

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, **options):
        threshold = time.time() - 5 * 86400  # 5 days
        for basedir, dirnames, filenames in os.walk(os.path.join(settings.MEDIA_ROOT, 'upload')):
            if filenames:
                for filename in filenames:
                    if os.stat(os.path.join(basedir, filename)).st_mtime < threshold:
                        os.unlink(os.path.join(basedir, filename))
                        os.rmdir(basedir)
