import base64
import datetime
import os
import shutil
import subprocess

import mutagen
import mutagen.mp3
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from ...models import SoundFile


def get_bitrate(filename):
    p = subprocess.Popen(
        ['mediainfo', '--Inform=Audio;%BitRate%', filename],
        close_fds=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = p.communicate()
    try:
        return int(stdout) / 1000
    except ValueError:
        return None


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--force',
            action='store_true',
            dest='force',
            default=False,
            help='Create files even if they exist',
        )
        parser.add_argument(
            '--reset-metadata',
            action='store_true',
            dest='reset_metadata',
            default=False,
            help='Reset metadata on all files',
        )
        parser.add_argument(
            '--emission',
            dest='emission',
            metavar='EMISSION',
            default=None,
            help='Process files belonging to emission only',
        )
        parser.add_argument(
            '--episode',
            dest='episode',
            metavar='EPISODE',
            default=None,
            help='Process files belonging to episode only',
        )
        parser.add_argument(
            '--formats',
            dest='formats',
            default=getattr(settings, 'PODCAST_FILE_FORMATS', 'ogg,mp3'),
            help='File formats',
        )
        parser.add_argument(
            '--copy', action='store_true', dest='copy', default=False, help='Copy initial file'
        )
        parser.add_argument(
            '--link', action='store_true', dest='link', default=False, help='Link initial file'
        )
        parser.add_argument(
            '--newer',
            dest='newer',
            help='Limit to soundfiles newer than (either a number of a second, '
            'or a number of days when suffixed by "d"',
        )

    def handle(
        self, force, reset_metadata, copy, link, emission, episode, verbosity, formats, newer, **kwargs
    ):
        self.verbose = verbosity > 1
        self.copy = copy
        self.link = link

        qs = SoundFile.objects.all()

        if emission:
            qs = qs.filter(episode__emission__slug=emission)

        if episode:
            qs = qs.filter(episode__slug=episode)

        if newer:
            if newer.endswith('d'):
                newer = int(newer.strip('d')) * 86400
            else:
                newer = int(newer)
            qs = qs.filter(creation_timestamp__gt=timezone.now() - datetime.timedelta(seconds=newer))

        for soundfile in qs.select_related():
            try:
                if soundfile.file is None or not os.path.exists(soundfile.file.path):
                    continue
            except ValueError:  # no file associated with it
                continue
            if not soundfile.podcastable:
                # get duration using initial file
                if not soundfile.duration:
                    soundfile.compute_duration()
                    if soundfile.duration:
                        soundfile.save()
                continue
            for format in formats.split(','):
                file_path = soundfile.get_format_path(format)
                created = False
                if not os.path.exists(file_path) or force:
                    created = self.create(soundfile, format)
                if created or reset_metadata:
                    self.set_metadata(soundfile, format)
            if force or not soundfile.duration:
                soundfile.compute_duration()
                if soundfile.duration:
                    soundfile.save()
            if force or not soundfile.mp3_file_size:
                file_path = soundfile.get_format_path('mp3')
                if os.path.exists(file_path):
                    soundfile.mp3_file_size = os.stat(file_path).st_size
                    soundfile.save()

    def create(self, soundfile, format):
        file_path = soundfile.get_format_path(format)
        if not os.path.exists(os.path.dirname(file_path)):
            os.makedirs(os.path.dirname(file_path))

        if self.copy and os.path.splitext(soundfile.file.path)[-1].strip('.') == format:
            shutil.copy(soundfile.file.path, file_path)
            return

        if self.link and os.path.splitext(soundfile.file.path)[-1].strip('.') == format:
            os.symlink(soundfile.file.path, file_path)
            return

        vorbis_rates = [64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 500]
        orig_bitrate = get_bitrate(soundfile.file.path)
        vorbis_q = 5
        if orig_bitrate is not None:
            if soundfile.episode.emission.podcast_sound_quality == 'highest':
                vorbis_q = 9
            elif soundfile.episode.emission.podcast_sound_quality == 'high':
                vorbis_q = 7
            # cap quality to original bitrate (using vorbis quality mapping)
            for i, rate in enumerate(vorbis_rates):
                if orig_bitrate < rate:
                    vorbis_q = min((i, vorbis_q))
                    break

        cmd = ['ffmpeg', '-y', '-i', soundfile.file.path, '-vn']
        if format == 'ogg':
            cmd.extend(['-q:a', str(vorbis_q)])
        elif format == 'mp3':
            mp3_q = 9 - vorbis_q
            if mp3_q == 0:
                cmd.extend(['-b:a', '320k'])
            else:
                cmd.extend(['-q:a', str(mp3_q)])

        # create soundfile under a temporary name to avoid moment where a part file
        # exists and may be mentioned in a RSS feed and taken up on a platform where
        # it will stay incomplete because they won't update for no reason
        # (e.g. spotify).
        tmp_file_path = file_path + '.tmp.%s' % format
        cmd.append(tmp_file_path)

        if self.verbose:
            print('creating', file_path)
            print('  ', ' '.join(cmd))
        else:
            cmd[1:1] = ['-loglevel', 'quiet']

        subprocess.call(cmd)
        if not os.path.exists(tmp_file_path):
            return False

        if os.path.exists(file_path):
            os.unlink(file_path)
        os.rename(tmp_file_path, file_path)
        return True

    def set_metadata(self, soundfile, format):
        file_path = soundfile.get_format_path(format)

        audio = mutagen.File(file_path, easy=True)
        audio.delete()  # strip all existing metadata

        if soundfile.fragment is True and soundfile.title:
            audio['title'] = '%s - %s' % (soundfile.episode.title, soundfile.title)
        else:
            audio['title'] = soundfile.episode.title
        audio['album'] = soundfile.episode.emission.title
        audio['artist'] = settings.RADIO_NAME

        if soundfile.episode.image or soundfile.episode.emission.image:
            image = soundfile.episode.image or soundfile.episode.emission.image
            image.file.open()
            if os.path.splitext(image.path)[1].lower() in ('.jpeg', '.jpg'):
                mimetype = 'image/jpeg'
            elif os.path.splitext(image.path)[1].lower() == '.png':
                mimetype = 'image/png'
            else:
                mimetype = None
            if mimetype:
                if format == 'ogg':
                    audio['coverartmime'] = mimetype
                    audio['coverartdescription'] = 'image'
                    audio['coverart'] = base64.encodebytes(image.read()).replace(b'\n', b'').decode('ascii')
                elif format == 'mp3':
                    audio.save()
                    audio = mutagen.mp3.MP3(file_path)
                    audio.tags.add(
                        mutagen.id3.APIC(
                            encoding=3, description='image', type=3, mime=mimetype, data=image.read()
                        )
                    )
            image.close()

        audio.save()
