import datetime
import json
import os
import subprocess

import numpy as np
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from ...models import SoundFile


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--force',
            action='store_true',
            dest='force',
            default=False,
            help='Create files even if they exist',
        )
        parser.add_argument(
            '--emission',
            dest='emission',
            metavar='EMISSION',
            default=None,
            help='Process files belonging to emission only',
        )
        parser.add_argument(
            '--episode',
            dest='episode',
            metavar='EPISODE',
            default=None,
            help='Process files belonging to episode only',
        )
        parser.add_argument(
            '--newer',
            dest='newer',
            help='Limit to soundfiles newer than (either a number of a second, '
            'or a number of days when suffixed by "d"',
        )

    def handle(self, force, emission, episode, newer, verbosity, **kwargs):
        self.verbose = verbosity > 1

        qs = SoundFile.objects.exclude(podcastable=False)

        if emission:
            qs = qs.filter(episode__emission__slug=emission)

        if episode:
            qs = qs.filter(episode__slug=episode)

        if newer:
            if newer.endswith('d'):
                newer = int(newer.strip('d')) * 86400
            else:
                newer = int(newer)
            qs = qs.filter(creation_timestamp__gt=timezone.now() - datetime.timedelta(seconds=newer))

        for soundfile in qs.select_related():
            try:
                if soundfile.file is None or not os.path.exists(soundfile.file.path):
                    continue
            except ValueError:  # no file associated with it
                continue
            file_path = soundfile.get_format_path('waveform.json')
            if os.path.exists(file_path) and not force:
                continue
            self.create(soundfile)

    def create(self, soundfile):
        # create a raw output, with a single channel and 4000Hz samples of 8bits
        sound_filename = soundfile.get_format_path('ogg')
        if not os.path.exists(sound_filename):
            sound_filename = soundfile.get_format_path('mp3')
        if not os.path.exists(sound_filename):
            return
        cmd = [
            'sox',
            sound_filename,
            '-t',
            'raw',
            '-c',
            '1',
            '-r',
            '4000',
            '-e' 'unsigned-integer',
            '-b',
            '8',
            '-',
        ]
        wave_array = subprocess.check_output(cmd)
        for filename, n_samples in (('waveform.json', 200), ('waveform-500.json', 500)):
            file_path = soundfile.get_format_path(filename)
            if self.verbose:
                print('creating', file_path)
            # reduce to $n_samples$ samples of max positive value
            wave_reduced = [
                int(max(x))
                for x in np.array_split(np.array([max(0, x - 128) for x in wave_array]), n_samples)
            ]
            json.dump(wave_reduced, open(file_path, 'w'))
