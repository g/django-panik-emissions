from datetime import datetime

from django.core.management.base import BaseCommand, CommandError

from ...utils import day_program


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('date', metavar='DATE', type=str)

    def handle(self, date, **options):
        year, month, day = date.split('-')
        date = datetime(int(year), int(month), int(day))
        for entry in day_program(date):
            print(entry.datetime, entry)
