import json

from django.core import serializers
from django.core.management.base import BaseCommand

from emissions.models import Emission


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('emission_slug', metavar='EMISSION')

    def handle(self, emission_slug, **options):
        emission = Emission.objects.get(slug=emission_slug)
        export = json.loads(
            serializers.serialize(
                'json', [emission], use_natural_foreign_keys=True, use_natural_primary_keys=True
            )
        )[0].get('fields')
        export['episodes'] = []
        for episode in emission.episode_set.all():
            episode_dict = json.loads(serializers.serialize('json', [episode]))[0].get('fields')
            del episode_dict['emission']
            episode_dict['diffusions'] = []
            for diffusion in episode.diffusion_set.all():
                diffusion_dict = json.loads(serializers.serialize('json', [diffusion]))[0].get('fields')
                del diffusion_dict['episode']
                episode_dict['diffusions'].append(diffusion_dict)
            export['episodes'].append(episode_dict)
        export['newsitems'] = []
        for newsitem in emission.newsitem_set.all():
            newsitem_dict = json.loads(serializers.serialize('json', [newsitem]))[0].get('fields')
            del newsitem_dict['emission']
            export['newsitems'].append(newsitem_dict)
        export['schedules'] = []
        for schedule in emission.schedule_set.all():
            schedule_dict = json.loads(serializers.serialize('json', [schedule]))[0].get('fields')
            del schedule_dict['emission']
            export['schedules'].append(schedule_dict)
        print(json.dumps(export, indent=2))
