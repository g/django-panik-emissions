import json

from django.core import serializers
from django.core.management.base import BaseCommand
from django.db import transaction

from emissions.models import Emission


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME')

    def handle(self, filename, **options):
        with open(filename) as fd:
            emission_dict = {'model': 'emissions.emission', 'fields': json.load(fd)}
        with transaction.atomic():
            emission = next(
                serializers.deserialize('json', json.dumps([emission_dict]), ignorenonexistent=True)
            )
            Emission.objects.filter(slug=emission.object.slug).delete()
            emission.save()
            for episode_subdict in emission_dict['fields']['episodes']:
                episode_dict = {'model': 'emissions.episode', 'fields': episode_subdict}
                episode = next(
                    serializers.deserialize('json', json.dumps([episode_dict]), ignorenonexistent=True)
                )
                episode.object.emission = emission.object
                episode.save()
                for diffusion_subdict in episode_subdict['diffusions']:
                    diffusion_dict = {'model': 'emissions.diffusion', 'fields': diffusion_subdict}
                    diffusion = next(
                        serializers.deserialize('json', json.dumps([diffusion_dict]), ignorenonexistent=True)
                    )
                    diffusion.object.episode = episode.object
                    diffusion.save()
            for newsitem_subdict in emission_dict['fields']['newsitems']:
                newsitem_dict = {'model': 'emissions.newsitem', 'fields': newsitem_subdict}
                newsitem = next(
                    serializers.deserialize('json', json.dumps([newsitem_dict]), ignorenonexistent=True)
                )
                newsitem.object.emission = emission.object
                newsitem.save()
            for schedule_subdict in emission_dict['fields']['schedules']:
                schedule_dict = {'model': 'emissions.schedule', 'fields': schedule_subdict}
                schedule = next(
                    serializers.deserialize('json', json.dumps([schedule_dict]), ignorenonexistent=True)
                )
                schedule.object.emission = emission.object
                schedule.save()
