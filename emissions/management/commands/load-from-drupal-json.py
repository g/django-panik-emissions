import datetime
import json
import os
from io import BytesIO

import isodate
import requests
from django.conf import settings
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify
from django.utils.timezone import make_naive

from ...models import Diffusion, Emission, Episode, SoundFile


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME', type=str, help='name of file to import')

    def adjust_slug(self, title):
        slug = slugify(title.replace("'", " ").replace("’", " "))
        if slug == 'quand-la-musique-est-bof':
            slug = 'quand-la-musique-est-b-o-f'
        elif slug == 'trad53-transmission-directe':
            slug = 'trad-53-transmission-directe'
        elif slug == 'tranzistor':
            slug = 'tranzistor-l-emission'
        return slug.strip('-')

    def handle(self, filename, verbosity, **options):
        self.verbose = verbosity > 1
        values = json.load(open(filename)).get('episodes').values()
        for i, episode_data in enumerate(values):
            if not episode_data.get('title'):
                continue
            if not episode_data.get('emission'):
                continue
            if not episode_data.get('text') and not episode_data.get('sound'):
                continue
            if self.verbose:
                print('[%5d/%5d] %s' % (i + 1, len(values), episode_data['title']))
            emission_slug = self.adjust_slug(episode_data['emission'])
            emission, created = Emission.objects.get_or_create(
                slug=emission_slug, defaults={'title': episode_data['emission']}
            )
            if created:
                emission.archived = True
                emission.save()
            episode, created = Episode.objects.get_or_create(
                slug=episode_data['path'].split('/')[-1], emission=emission
            )
            episode.title = episode_data['title']
            episode.text = episode_data['text']
            episode.creation_timestamp = isodate.parse_datetime(
                episode_data['pub_date'].replace(' ', 'T', 1).replace(' ', '')
            )
            if not settings.USE_TZ:
                episode.creation_timestamp = make_naive(episode.creation_timestamp)
            start_datetime = datetime.datetime.combine(
                datetime.datetime.strptime(episode_data['start_date'], '%d/%m/%Y').date(),
                datetime.datetime.strptime(episode_data['start_time'], '%H:%M').time(),
            )
            if episode_data['end_time']:
                end_datetime = datetime.datetime.combine(
                    start_datetime.date(),
                    datetime.datetime.strptime(episode_data['end_time'], '%H:%M').time(),
                )
            else:
                end_datetime = start_datetime  # fake
            if end_datetime < start_datetime:
                end_datetime = end_datetime + datetime.timedelta(days=1)
            episode.duration = (end_datetime - start_datetime).seconds // 60
            episode.save()
            if not episode.image and episode_data.get('image'):
                orig_path = 'images/%s/%s' % (emission.slug, episode_data.get('image').split('/')[-1])
                if default_storage.exists(orig_path):
                    path = orig_path
                else:
                    episode.image = None
                    episode.save()
                    response = requests.get(episode_data.get('image'))
                    if response.status_code == 200:
                        path = default_storage.save(
                            'images/%s/%s' % (emission.slug, episode_data.get('image').split('/')[-1]),
                            ContentFile(response.content),
                        )
                        episode.image = default_storage.open(path)
                try:
                    episode.save()
                except OSError:  # OSError: cannot identify image file
                    episode.image = None
                    episode.save()
            if episode.diffusion_set.count() == 0:
                diffusion = Diffusion(episode=episode)
                diffusion.datetime = start_datetime
                diffusion.save()
            if episode_data.get('sound') and not episode.has_sound():
                orig_path = os.path.join(
                    settings.MEDIA_ROOT,
                    'sounds.orig/%s/%s' % (emission.slug, episode_data.get('sound').split('/')[-1]),
                )
                if not os.path.exists(orig_path):
                    response = requests.get(episode_data.get('sound'))
                    if response.status_code == 200:
                        bio = BytesIO(response.content)
                        soundfile = SoundFile(
                            episode=episode, podcastable=True, fragment=False, title=episode.title
                        )
                        soundfile.file = File(bio)
                        soundfile.file.name = episode_data.get('sound').split('/')[-1]
                        soundfile.save()
