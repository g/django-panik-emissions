import gzip
import os
import re
import time
import xml.etree.ElementTree as ET
from datetime import datetime
from optparse import make_option

import urllib2
from _spip2html import makeHtmlFromSpip
from django.conf import settings
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse
from django.utils.html import strip_tags
from django.utils.text import slugify
from PIL import Image

from ...models import (
    Category,
    Diffusion,
    Emission,
    Episode,
    NewsCategory,
    NewsItem,
    SoundFile,
    get_sound_path,
)


class Rubric:
    archived = False

    spip_url_marker = '-'
    spip_url_object_name = 'rubrique'

    def __init__(self):
        self.articles = {}
        self.rubrics = {}
        self.categories = []

    @property
    def id(self):
        return self.id_rubrique


class Article:
    spip_url_marker = ''
    spip_url_object_name = 'article'

    url = None
    skip_import = False
    is_actually_a_newsitem = False

    def __init__(self):
        self.news_categories = []

    @property
    def id(self):
        return self.id_article


class Breve:
    spip_url_marker = '+'
    spip_url_object_name = 'breve'

    emission = None

    def __init__(self):
        self.news_categories = []
        self.mots_cles = []

    @property
    def id(self):
        return self.id_breve


class KeywordGroup:
    def __init__(self):
        self.keywords = {}


class Keyword:
    related_object = None


class Document:
    pass


def web_image(filepath):
    # check filepath for size and dimensions, and reduce to reasonable values
    # if necessary

    orig_filepath = os.path.join(settings.MEDIA_ROOT, 'IMG', filepath)
    try:
        image = Image.open(orig_filepath)
    except:
        return filepath
    if image.size[0] <= 1000 and image.size[1] <= 1000:
        return filepath

    filehead, filetail = os.path.split(filepath)
    basename, format = os.path.splitext(filetail)
    miniature_filename = os.path.join(filehead, basename + '__ok.jpeg')
    miniature_filepath = os.path.join(settings.MEDIA_ROOT, 'IMG', miniature_filename)

    if os.path.exists(miniature_filepath) and os.path.getmtime(orig_filepath) > os.path.getmtime(
        miniature_filepath
    ):
        os.unlink(miniature_filepath)

    # if the image wasn't already resized, resize it
    if not os.path.exists(miniature_filepath) and os.path.exists(orig_filepath):
        if image.size[0] > 1000:
            x = 1000
            y = int(image.size[1] * 1.0 * x / image.size[0])
        elif image.size[1] > 1000:
            y = 1000
            x = int(image.size[0] * 1.0 * y / image.size[1])

        image = image.resize([x, y], Image.ANTIALIAS)
        try:
            image.save(miniature_filepath, image.format, quality=90, optimize=1)
        except:
            image.save(miniature_filepath, image.format, quality=90)

    return miniature_filename


class Command(BaseCommand):
    args = 'filename'
    help = 'Load emissions and episodes from a Spip dump file'

    option_list = BaseCommand.option_list + (
        make_option(
            '--no-updates',
            action='store_true',
            dest='dont_update',
            default=False,
            help='Only create new objects, do not update existing ones',
        ),
        make_option(
            '--stats-skipped',
            dest='stats_skipped_file',
            metavar='FILE',
            default=None,
            help='Create a CSV file ith skipped articles',
        ),
        make_option(
            '--rewritemap',
            dest='rewritemap_file',
            metavar='FILE',
            default=None,
            help='Create a Apache RewriteMap',
        ),
        make_option(
            '--article', dest='article_id', metavar='ARTICLE_ID', default='', help='Process only that article'
        ),
    )

    def handle(
        self, filename, dont_update, rewritemap_file, stats_skipped_file, article_id, verbosity, **options
    ):
        self.verbose = verbosity > 1
        self.article_ids = article_id.split(',')
        self.do_updates = not dont_update
        if rewritemap_file:
            self.rewritemap = []
        if stats_skipped_file:
            self.stats_skipped_file = file(stats_skipped_file, 'w')
        else:
            self.stats_skipped_file = None

        with open(filename) as fd:
            if self.verbose:
                print('Reading SPIP dump file')
            content = fd.read()
            # the spip_courriers parts of the spip export are not properly
            # encoded, we manually remove them here so the XML file can be
            # parsed correctly.
            content = (
                content[: content.find('<spip_courriers>')]
                + content[content.rfind('</spip_courriers>') + 17 :]
            )
            self.root = ET.fromstring(content)

            if self.verbose:
                print('Loading objects from dump')
            self.load_keyword_groups()
            self.load_keywords()
            self.load_rubrics()

            emission_rubric_ids = []
            straight_emission_rubric_ids = []
            for rubric in self.rubrics['2'].rubrics.values():  # 'Les emissions'
                emission_rubric_ids.append(rubric.id_rubrique)
                straight_emission_rubric_ids.append(rubric.id_rubrique)
                for subrubric in rubric.rubrics.values():
                    emission_rubric_ids.append(subrubric.id_rubrique)

            self.breves = {}
            if not self.article_ids:
                if self.verbose:
                    print('  breves...')
                self.load_breves()

            if self.verbose:
                print('  articles...')
            self.load_articles(emission_rubric_ids)

            self.set_urls()

            self.load_documents()
            self.load_document_links()

            self.process_emission_keywords()
            self.process_episode_keywords()
            self.process_newsitem_keywords()

            print('Creating emissions')
            for emission_id in straight_emission_rubric_ids:
                rubric = self.rubrics[emission_id]
                emission = self.get_or_create_emission(rubric)
                print('  ', emission.title)
                if rewritemap_file:
                    self.add_rewritemap_entries(rubric, emission)

                for article in rubric.articles.values():
                    episode = self.get_or_create_episode(article, emission)
                    if episode is None:
                        continue
                    if rewritemap_file:
                        self.add_rewritemap_entries(article, episode)

                    self.set_sound_files(article, episode)

            print('Creating newsitems')
            for breve in self.breves.values():
                if breve.titre.startswith('Cette semaine sur Panik'):
                    continue
                newsitem = self.get_or_create_newsitem(breve)
                if rewritemap_file:
                    self.add_rewritemap_entries(breve, newsitem)

        if rewritemap_file:
            self.write_rewritemap(rewritemap_file)

        if self.stats_skipped_file:
            self.stats_skipped_file.close()

    def load_keyword_groups(self):
        self.keyword_groups = {}
        for keywordgroup_xml in self.root.iter('spip_groupes_mots'):
            keyword_group = KeywordGroup()
            for attr in ('id_groupe', 'titre'):
                setattr(keyword_group, attr, keywordgroup_xml.find(attr).text)
            if keyword_group.id_groupe not in (
                '11',  # archives
                '12',  # subjects
                '3',  # category
                '10',  # transversal
                '14',  # news categories
                '4',  # "special articles"
            ):
                continue
            self.keyword_groups[keyword_group.id_groupe] = keyword_group

    def load_keywords(self):
        self.keywords = {}
        for keyword_xml in self.root.iter('spip_mots'):
            keyword = Keyword()
            for attr in ('id_mot', 'titre', 'id_groupe'):
                setattr(keyword, attr, keyword_xml.find(attr).text)
            if not keyword.id_groupe in self.keyword_groups:
                continue
            if keyword.id_mot in ('92',):  # blacklist
                continue
            self.keywords[keyword.id_mot] = keyword
            self.keyword_groups[keyword.id_groupe] = keyword

    def load_rubrics(self):
        self.rubrics = {}
        for rubric_xml in self.root.iter('spip_rubriques'):
            rubric = Rubric()
            for attr in ('id_rubrique', 'id_parent', 'titre', 'descriptif', 'texte'):
                setattr(rubric, attr, rubric_xml.find(attr).text)
            self.rubrics[rubric.id_rubrique] = rubric

        for rubric in self.rubrics.values():
            if rubric.id_parent and rubric.id_parent != '0':
                self.rubrics[rubric.id_parent].rubrics[rubric.id_rubrique] = rubric

    def load_breves(self):
        for breve_xml in self.root.iter('spip_breves'):
            breve = Breve()
            for attr in ('id_breve', 'titre', 'texte', 'date_heure', 'statut'):
                setattr(breve, attr, breve_xml.find(attr).text)
            if breve.statut != 'publie':
                continue
            self.breves[breve.id_breve] = breve

    def load_articles(self, emission_rubric_ids):
        self.articles = {}

        actually_newsitems = {}
        for articlekeyword_xml in self.root.iter('spip_mots_articles'):
            keyword_id = articlekeyword_xml.find('id_mot').text
            article_id = articlekeyword_xml.find('id_article').text
            keyword = self.keywords.get(keyword_id)
            if keyword is None:
                continue
            if keyword.id_groupe == '14':  # news categories
                actually_newsitems[article_id] = keyword

        for article_xml in self.root.iter('spip_articles'):
            article = Article()
            for attr in (
                'id_rubrique',
                'id_article',
                'titre',
                'surtitre',
                'soustitre',
                'descriptif',
                'chapo',
                'texte',
                'date_redac',
                'statut',
                'date',
            ):
                setattr(article, attr, article_xml.find(attr).text)

            if self.article_ids and article.id_article not in self.article_ids:
                continue

            if article_xml.find('id_rubrique').text == '65':
                pass  # rubric for events, handle with care
            elif not article_xml.find('id_rubrique').text in emission_rubric_ids:
                keyword = actually_newsitems.get(article.id_article)
                if keyword is None:
                    continue
                article.is_actually_a_newsitem = True
                article.news_categories.append(keyword)

            if article.id_rubrique == '65' or article.is_actually_a_newsitem:
                # this is an event, they get a special handling, to be
                # merged with newsitems
                if article.statut not in ('publie', 'prop'):
                    continue
                self.turn_article_in_breve(article)
                continue

            if article.statut != 'publie':
                continue
            article.mots_cles = []
            self.articles[article.id_article] = article

            if self.rubrics[article.id_rubrique].id_parent != '2':
                # the spip structure didn't really expect subrubrics in the
                # 'emissions' section, but people added some nevertheless,
                # move related articles to their parent rubric.
                article.id_rubrique = self.rubrics[article.id_rubrique].id_parent

            self.rubrics[article.id_rubrique].articles[article.id_article] = article

    def set_urls(self):
        for spip_url_xml in self.root.iter('spip_urls'):
            id_objet = spip_url_xml.find('id_objet').text
            url = spip_url_xml.find('url').text
            if spip_url_xml.find('type').text == 'article' and id_objet in self.articles:
                self.articles[id_objet].url = url
            elif spip_url_xml.find('type').text == 'article' and ('0%s' % id_objet) in self.breves:
                self.breves['0' + id_objet].url = url
            elif spip_url_xml.find('type').text == 'rubrique' and id_objet in self.rubrics:
                self.rubrics[id_objet].url = url
            elif spip_url_xml.find('type').text == 'mot' and id_objet in self.keywords:
                self.keywords[id_objet].url = url
            elif spip_url_xml.find('type').text == 'breve' and id_objet in self.breves:
                self.breves[id_objet].url = url

    def load_documents(self):
        self.documents = {}
        for spip_doc_xml in self.root.iter('spip_documents'):
            id_document = spip_doc_xml.find('id_document').text
            doc = Document()
            doc.filename = spip_doc_xml.find('fichier').text
            doc.title = spip_doc_xml.find('titre').text
            if spip_doc_xml.find('distant').text == 'oui':
                url = doc.filename
                doc.filename = os.path.split(url)[-1]
                filename = os.path.join('media/IMG/', doc.filename)
                if not os.path.exists('media/IMG'):
                    continue
                if not os.path.exists(filename):
                    fd = file(filename, 'w')
                    fd.write(urllib2.urlopen(url).read())
                    fd.close()
            self.documents[id_document] = doc

    def load_document_links(self):
        self.attached_documents = {}
        for spip_doc_liens_xml in self.root.iter('spip_documents_liens'):
            id_document = spip_doc_liens_xml.find('id_document').text
            id_object = spip_doc_liens_xml.find('id_objet').text
            # TODO: check what's this spip attribute
            # if spip_doc_liens_xml.find('vu').text == 'non':
            #    continue
            if spip_doc_liens_xml.find('objet').text != 'article':
                continue
            if not self.attached_documents.get(id_object):
                self.attached_documents[id_object] = []
            self.attached_documents[id_object].append(self.documents.get(id_document))

    def process_emission_keywords(self):
        for rubrickeyword_xml in self.root.iter('spip_mots_rubriques'):
            keyword_id = rubrickeyword_xml.find('id_mot').text
            rubric_id = rubrickeyword_xml.find('id_rubrique').text
            rubric = self.rubrics.get(rubric_id)
            if not rubric:
                continue

            if keyword_id == '100':  # archive
                rubric.archived = True
                continue

            keyword = self.keywords.get(keyword_id)
            if keyword is None:
                continue

            if keyword.id_groupe == '3':  # category
                rubric.categories.append(keyword)
                if not keyword.related_object:
                    cs = Category.objects.filter(title=keyword.titre)
                    if len(cs):
                        c = cs[0]
                    else:
                        c = Category()
                    c.title = keyword.titre
                    c.save()
                    keyword.related_object = c

    def process_episode_keywords(self):
        for articlekeyword_xml in self.root.iter('spip_mots_articles'):
            keyword_id = articlekeyword_xml.find('id_mot').text
            article_id = articlekeyword_xml.find('id_article').text

            article = self.articles.get(article_id)
            if not article:
                article = self.breves.get('0%s' % article_id)
                if not article:
                    continue

            keyword = self.keywords.get(keyword_id)
            if keyword is None:
                continue

            if keyword.id_groupe in ('10', '12'):  # transversales & sujets
                article.mots_cles.append(keyword.titre)

            if keyword.id_groupe == '14':  # news categories
                article.is_actually_a_newsitem = True
                article.news_categories.append(keyword)
                if not keyword.related_object:
                    cs = NewsCategory.objects.filter(title=keyword.titre)
                    if len(cs):
                        c = cs[0]
                    else:
                        c = NewsCategory()
                    c.title = keyword.titre
                    c.slug = slugify(unicode(c.title))
                    c.save()
                    keyword.related_object = c

            if keyword_id == '118':  # actu
                article.is_actually_a_newsitem = True
            elif keyword.id_groupe == '4':  # "special" articles
                article.skip_import = True

    def process_newsitem_keywords(self):
        for brevekeyword_xml in self.root.iter('spip_mots_breves'):
            keyword_id = brevekeyword_xml.find('id_mot').text
            breve_id = brevekeyword_xml.find('id_breve').text
            breve = self.breves.get(breve_id)
            if not breve:
                continue

            keyword = self.keywords.get(keyword_id)
            if keyword is None:
                continue

            if keyword.id_groupe in ('10', '12'):  # transversales & sujets
                breve.mots_cles.append(keyword.titre)

            if keyword.id_groupe == '14':  # news categories
                breve.news_categories.append(keyword)
                if not keyword.related_object:
                    cs = NewsCategory.objects.filter(title=keyword.titre)
                    if len(cs):
                        c = cs[0]
                    else:
                        c = NewsCategory()
                    c.title = keyword.titre
                    c.slug = slugify(unicode(c.title))
                    c.save()
                    keyword.related_object = c

    def get_or_create_emission(self, rubric):
        slug = rubric.url.lower()
        possible_slugs = [rubric.url.lower(), rubric.url.lower().split(',')[0]]
        for slug in possible_slugs:
            try:
                emission = Emission.objects.get(slug=slug)
                break
            except Emission.DoesNotExist:
                continue
        else:
            emission = Emission()

        emission.slug = possible_slugs[-1]

        if emission.id and (self.article_ids or not self.do_updates):
            return emission

        slug = slug.split(',')[0]
        emission.slug = slug
        emission.title = rubric.titre
        emission.archived = rubric.archived
        emission.text = makeHtmlFromSpip(rubric.texte or rubric.descriptif, documents=self.documents) or None
        if rubric.texte and rubric.descriptif and len(rubric.descriptif) < 50:
            # short descriptif, use it as subtitle
            subtitle = strip_tags(makeHtmlFromSpip(rubric.descriptif, simple=True, inline=True))
            if subtitle:
                emission.subtitle = subtitle

        image_path = None
        for ext in ('.jpg', '.png', '.gif'):
            if os.path.exists('media/IMG/rubon%s%s' % (rubric.id_rubrique, ext)):
                image_path = ['media/IMG/rubon%s%s' % (rubric.id_rubrique, ext)]
                break
        else:
            if emission.text:
                image_path = re.findall('src="/(media/IMG.*?)"', emission.text, re.DOTALL)

        self.set_image(emission, image_path)

        emission.save()
        emission.categories.clear()
        for category in rubric.categories:
            emission.categories.add(category.related_object)
        emission.save()
        return emission

    def turn_article_in_breve(self, article, emission=None):
        breve = Breve()
        breve.id_breve = '0%s' % article.id_article
        breve.titre = article.titre
        breve.texte = article.texte
        breve.date_heure = article.date
        breve.news_categories = article.news_categories
        if article.url:
            breve.url = article.url
        if emission:
            breve.emission = emission
        self.breves[breve.id_breve] = breve

    def get_or_create_episode(self, article, emission):
        if article.skip_import:
            return None

        if article.is_actually_a_newsitem:
            self.turn_article_in_breve(article, emission)
            return None

        if article.date_redac == '0000-00-00 00:00:00':
            # date_redac was used for the diffusion date, if it's
            # not set it's probably not really an episode
            if self.stats_skipped_file:
                episode_files = self.attached_documents.get(article.id_article)
                if episode_files:
                    has_sound = '♫'
                else:
                    has_sound = '-'
                base_spip_edit_url = 'http://www.radiopanik.org/spip/ecrire/?exec=articles_edit&id_article='
                self.stats_skipped_file.write(
                    unicode(
                        '%s\t%s\t%s\t%s%s'
                        % (emission.title, article.titre, has_sound, base_spip_edit_url, article.id_article)
                    ).encode('utf-8')
                )
            return None

        possible_slugs = [article.url.lower()]
        if article.url.lower().startswith('nouvel-article'):
            possible_slugs.append(slugify(unicode(article.titre))[:40] + '-%s' % article.id_article)

        for slug in possible_slugs:
            try:
                episode = Episode.objects.get(slug=slug)
                break
            except Episode.DoesNotExist:
                continue
        else:
            episode = Episode()

        episode.slug = possible_slugs[-1]

        if episode.id and not self.do_updates:
            return episode

        episode.emission = emission
        episode.title = article.titre
        episode.text = makeHtmlFromSpip(article.texte or article.descriptif, documents=self.documents) or None

        image_path = None
        for ext in ('.jpg', '.png', '.gif'):
            if os.path.exists('media/IMG/arton%s%s' % (article.id_article, ext)):
                image_path = ['media/IMG/arton%s%s' % (article.id_article, ext)]
                break
        else:
            if episode.text:
                image_path = re.findall('src="/(media/IMG.*?)"', episode.text, re.DOTALL)

        self.set_image(episode, image_path)
        self.set_portfolio(episode, article.id_article)

        episode.save()

        for motcle in article.mots_cles:
            episode.tags.add(motcle.lower())

        if not Diffusion.objects.filter(episode=episode).count():
            diffusion = Diffusion()
            diffusion.episode = episode
            try:
                diffusion.datetime = datetime.strptime(article.date_redac, '%Y-%m-%d %H:%M:%S')
            except ValueError:
                pass
            else:
                diffusion.save()

        return episode

    def set_sound_files(self, article, episode):
        if SoundFile.objects.filter(episode=episode).count():
            return  # skip episodes that already have sound files
        episode_files = self.attached_documents.get(article.id_article)
        if episode_files:
            for episode_file in episode_files:
                if episode_file is None:
                    continue
                if os.path.splitext(episode_file.filename)[-1] not in ('.ogg', '.mp3'):
                    continue
                if not os.path.exists('media/IMG/' + episode_file.filename):
                    continue
                soundfile = SoundFile()
                soundfile.episode = episode
                soundfile.podcastable = True
                soundfile.fragment = False
                soundfile.title = episode_file.title or '[pas de titre]'
                sound_path = os.path.join(
                    settings.MEDIA_ROOT, get_sound_path(soundfile, episode_file.filename)
                )
                if os.path.exists(sound_path):
                    os.unlink(sound_path)
                soundfile.file = File(file('media/IMG/' + episode_file.filename))
                soundfile.save()

    def get_or_create_newsitem(self, breve):
        slug = breve.url.lower()
        try:
            newsitem = NewsItem.objects.get(slug=slug)
        except NewsItem.DoesNotExist:
            newsitem = NewsItem()

        if newsitem.id and not self.do_updates:
            return newsitem

        newsitem.title = breve.titre
        newsitem.slug = slug
        newsitem.text = makeHtmlFromSpip(breve.texte, documents=self.documents) or None
        newsitem.date = datetime.strptime(breve.date_heure, '%Y-%m-%d %H:%M:%S')
        for ext in ('.jpg', '.png', '.gif'):
            if os.path.exists('media/IMG/breveon%s%s' % (breve.id_breve, ext)):
                image_path = ['media/IMG/breveon%s%s' % (breve.id_breve, ext)]
                break
        else:
            image_path = re.findall('src="/(media/IMG.*?)"', newsitem.text, re.DOTALL)

        self.set_image(newsitem, image_path)

        if breve.news_categories:
            newsitem.category = breve.news_categories[0].related_object

        if breve.id_breve[0] == '0':
            self.set_portfolio(newsitem, breve.id_breve[1:])

        newsitem.save()

        if breve.mots_cles:
            for motcle in breve.mots_cles:
                newsitem.tags.add(motcle.lower())

            newsitem.save()

    def set_image(self, object, image_path):
        if not image_path:
            return
        image_path = image_path[0]
        if not os.path.exists(image_path):
            return

        if object.text:
            # remove image from text
            img_in_text = re.findall('<img src="/%s".*?>' % image_path, object.text, re.DOTALL)
            if img_in_text:
                object.text = object.text.replace(img_in_text[0], '').replace('<p></p>', '')

        if object.image and os.path.basename(object.image.path) == os.path.basename(image_path):
            return
        object.image = File(file(image_path))

    def set_portfolio(self, object, id_objet):
        attached_files = self.attached_documents.get(id_objet)
        if not attached_files:
            return
        # exclude files that are already embedded in the text
        attached_files = [x for x in attached_files if x and x.filename not in object.text]
        if object.image:
            attached_files = [
                x
                for x in attached_files
                if os.path.splitext(os.path.basename(x.filename))[0].replace('__ok', '')
                not in object.image.path
            ]
        attached_files = [
            x for x in attached_files if os.path.splitext(x.filename)[-1] in ('.jpg', '.jpeg', '.png')
        ]

        if not attached_files:
            return

        if self.verbose:
            print('  set portfolio on', object)

        # get filename, eventually to properly sized images
        for i, attached_file in enumerate(attached_files):
            attached_files[i] = web_image(attached_file.filename)

        # and now, add them to the text
        object.text = (
            object.text
            + '\n'
            + '\n\n'.join(['<p><img src="/media/IMG/%s" alt=""/></p>' % x for x in attached_files])
        )

        # and then, call set_image if object has no image yet
        if not object.image:
            image_path = ['media/IMG/' + attached_files[0]]
            self.set_image(object, image_path)

    def add_rewritemap_entries(self, spip_object, django_object):
        if isinstance(django_object, Emission):
            object_url = reverse('emission-view', kwargs={'slug': django_object.slug})
        elif isinstance(django_object, Episode):
            object_url = reverse(
                'episode-view',
                kwargs={'slug': django_object.slug, 'emission_slug': django_object.emission.slug},
            )
        elif isinstance(django_object, NewsItem):
            object_url = reverse('news-view', kwargs={'slug': django_object.slug})
        else:
            return

        if spip_object.id[0] == '0':
            # our hack mapping some articles to newsitems
            spip_object.spip_url_object_name = 'article'
            spip_object.spip_url_marker = ''

        urls = []
        urls.append('%s%s' % (spip_object.spip_url_object_name, spip_object.id))
        urls.append('spip.php?%s%s' % (spip_object.spip_url_object_name, spip_object.id))
        urls.append('%s%s.html' % (spip_object.spip_url_object_name, spip_object.id))
        if spip_object.spip_url_object_name == 'article':
            urls.append(spip_object.id)
        urls.append('%s%s%s' % (spip_object.spip_url_marker, spip_object.url, spip_object.spip_url_marker))

        for url in urls:
            self.rewritemap.append((url, object_url))

    def write_rewritemap(self, rewritemap_file):
        fd = file(rewritemap_file, 'w')
        for src, dst in self.rewritemap:
            fd.write('%s %s\n' % (src, dst))
        fd.close()
