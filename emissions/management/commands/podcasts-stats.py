import urllib.parse

import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from emissions.models import SoundFile


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        resp = requests.get(urllib.parse.urljoin(settings.WEBSITE_BASE_URL, '__webstats/downloads.json'))
        for soundfile_id, download_count in resp.json().items():
            SoundFile.objects.filter(id=soundfile_id).update(download_count=download_count)
