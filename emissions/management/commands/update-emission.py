from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from ...models import Emission, Episode


class Command(BaseCommand):
    args = 'filename'
    help = 'Change emission of episodes'

    option_list = BaseCommand.option_list + (
        make_option('--emission', dest='emission_slug', metavar='SLUG', default=None),
    )

    def handle(self, filename, emission_slug, verbosity, **options):
        self.verbose = verbosity > 1
        emission = Emission.objects.get(slug=emission_slug)
        for line in file(filename):
            episode_slug = line.strip()
            try:
                episode = Episode.objects.get(slug=episode_slug)
            except Episode.DoesNotExist:
                print('missing episode:', episode)
            episode.emission = emission
            episode.save()
