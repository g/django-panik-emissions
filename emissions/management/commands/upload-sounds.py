import datetime
import os
import subprocess

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from ...models import SoundFile


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--emission',
            dest='emission',
            metavar='EMISSION',
            default=None,
            help='Limit to soundfiles belonging to emission (slug)',
        )
        parser.add_argument(
            '--episode',
            dest='episode',
            metavar='EPISODE',
            default=None,
            help='Limit to sounfiles belonging to episode (slug)',
        )
        parser.add_argument(
            '--newer',
            dest='newer',
            help='Limit to soundfiles newer than (either a number of a second, '
            'or a number of days when suffixed by "d"',
        )

    def handle(self, emission, episode, newer, verbosity, **kwargs):
        self.verbose = verbosity > 1

        qs = SoundFile.objects.exclude(podcastable=False)

        if emission:
            qs = qs.filter(episode__emission__slug=emission)

        if episode:
            qs = qs.filter(episode__slug=episode)

        if newer:
            if newer.endswith('d'):
                newer = int(newer.strip('d')) * 86400
            else:
                newer = int(newer)
            qs = qs.filter(creation_timestamp__gt=timezone.now() - datetime.timedelta(seconds=newer))

        for soundfile in qs.select_related():
            try:
                if soundfile.file is None or not os.path.exists(soundfile.file.path):
                    continue
            except ValueError:  # no file associated with it
                continue

            for file_format in ('ogg', 'mp3'):
                sound_filename = soundfile.get_format_path(file_format)
                assert sound_filename.startswith(settings.MEDIA_ROOT)
                if not os.path.exists(sound_filename):
                    continue
                dest = sound_filename[len(settings.MEDIA_ROOT) :]
                cmd = ['rsync', '-a', sound_filename, settings.WEBSITE_MEDIA_SOUNDS_SYNC_BASE + dest]
                if self.verbose:
                    cmd.insert(1, '-v')
                    print(dest)
                subprocess.run(cmd, check=True)
