from datetime import datetime, timedelta

from django.core.management.base import BaseCommand, CommandError
from django.views.generic.dates import _date_from_string

from ...utils import period_program


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('year', metavar='YEAR', type=str)
        parser.add_argument('week', metavar='WEEK', type=str)

    def handle(self, year, week, **options):
        date = _date_from_string(year, '%Y', '1', '%w', week, '%W')
        date = datetime(*date.timetuple()[:3])
        for entry in period_program(date, date + timedelta(days=7)):
            print(entry.datetime, entry)
