import datetime

from django.core.management.base import BaseCommand

from ...utils import whatsonair


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('date_time', metavar='DATETIME', nargs='?', type=str, help='ex: 2022-06-30T14:53')

    def handle(self, date_time, **options):
        now = datetime.datetime.strptime(date_time, '%Y-%m-%dT%H:%M') if date_time else None
        onair = whatsonair(now)
        print('found episode:', onair.get('episode'))
        print('found emission:', onair.get('emission'))
        print('found nonstop:', onair.get('nonstop'))
