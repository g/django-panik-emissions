import ckeditor.fields
import taggit.managers
from django.db import migrations, models

import emissions.models


class Migration(migrations.Migration):
    dependencies = [
        ('taggit', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Absence',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('datetime', models.DateTimeField(verbose_name='Date/time', db_index=True)),
            ],
            options={
                'verbose_name': 'Absence',
                'verbose_name_plural': 'Absences',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(null=True)),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Colour',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(null=True)),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Colour',
                'verbose_name_plural': 'Colours',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Diffusion',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('datetime', models.DateTimeField(verbose_name='Date/time', db_index=True)),
            ],
            options={
                'verbose_name': 'Diffusion',
                'verbose_name_plural': 'Diffusions',
            },
            bases=(models.Model, emissions.models.WeekdayMixin),
        ),
        migrations.CreateModel(
            name='Emission',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('slug', models.SlugField(max_length=200)),
                ('subtitle', models.CharField(max_length=80, null=True, verbose_name='Subtitle', blank=True)),
                ('text', ckeditor.fields.RichTextField(null=True, verbose_name='Description')),
                ('archived', models.BooleanField(default=False, verbose_name='Archived')),
                (
                    'duration',
                    models.IntegerField(default=60, help_text='In minutes', verbose_name='Duration'),
                ),
                ('email', models.EmailField(max_length=254, null=True, verbose_name='Email', blank=True)),
                ('website', models.URLField(null=True, verbose_name='Website', blank=True)),
                (
                    'image',
                    models.ImageField(
                        max_length=250,
                        upload_to=emissions.models.get_image_path,
                        null=True,
                        verbose_name='Image',
                        blank=True,
                    ),
                ),
                ('got_focus', models.DateTimeField(default=None, null=True, blank=True)),
                ('has_focus', models.BooleanField(default=False)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                (
                    'categories',
                    models.ManyToManyField(
                        to='emissions.Category', null=True, verbose_name='Categories', blank=True
                    ),
                ),
                (
                    'colours',
                    models.ManyToManyField(
                        to='emissions.Colour', null=True, verbose_name='Colours', blank=True
                    ),
                ),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Emission',
                'verbose_name_plural': 'Emissions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Episode',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('slug', models.SlugField(max_length=200)),
                (
                    'subtitle',
                    models.CharField(max_length=150, null=True, verbose_name='Subtitle', blank=True),
                ),
                ('text', ckeditor.fields.RichTextField(null=True, verbose_name='Description')),
                (
                    'duration',
                    models.IntegerField(
                        help_text='In minutes', null=True, verbose_name='Duration', blank=True
                    ),
                ),
                (
                    'image',
                    models.ImageField(
                        max_length=250,
                        upload_to=emissions.models.get_image_path,
                        null=True,
                        verbose_name='Image',
                        blank=True,
                    ),
                ),
                ('got_focus', models.DateTimeField(default=None, null=True, blank=True)),
                ('has_focus', models.BooleanField(default=False)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                (
                    'emission',
                    models.ForeignKey(
                        verbose_name='Emission', to='emissions.Emission', on_delete=models.CASCADE
                    ),
                ),
                (
                    'tags',
                    taggit.managers.TaggableManager(
                        to='taggit.Tag',
                        through='taggit.TaggedItem',
                        blank=True,
                        help_text='A comma-separated list of tags.',
                        verbose_name='Tags',
                    ),
                ),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Episode',
                'verbose_name_plural': 'Episodes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Focus',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'title',
                    models.CharField(max_length=50, null=True, verbose_name='Alternate Title', blank=True),
                ),
                ('current', models.BooleanField(default=True, verbose_name=b'Current')),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                (
                    'emission',
                    models.ForeignKey(
                        verbose_name='Emission',
                        blank=True,
                        to='emissions.Emission',
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'episode',
                    models.ForeignKey(
                        verbose_name='Episode',
                        blank=True,
                        to='emissions.Episode',
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Format',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(null=True)),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Format',
                'verbose_name_plural': 'Formats',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsCategory',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(null=True)),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'News Category',
                'verbose_name_plural': 'News Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsItem',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('slug', models.SlugField(max_length=200)),
                ('text', ckeditor.fields.RichTextField(verbose_name='Description')),
                ('date', models.DateField(verbose_name='Publication Date')),
                (
                    'image',
                    models.ImageField(
                        max_length=250,
                        upload_to=emissions.models.get_image_path,
                        null=True,
                        verbose_name='Image',
                        blank=True,
                    ),
                ),
                ('expiration_date', models.DateField(null=True, verbose_name='Expiration Date', blank=True)),
                ('got_focus', models.DateTimeField(default=None, null=True, blank=True)),
                ('has_focus', models.BooleanField(default=False)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                (
                    'category',
                    models.ForeignKey(
                        verbose_name='Category',
                        blank=True,
                        to='emissions.NewsCategory',
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
                (
                    'emission',
                    models.ForeignKey(
                        verbose_name='Emission',
                        blank=True,
                        to='emissions.Emission',
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'tags',
                    taggit.managers.TaggableManager(
                        to='taggit.Tag',
                        through='taggit.TaggedItem',
                        blank=True,
                        help_text='A comma-separated list of tags.',
                        verbose_name='Tags',
                    ),
                ),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'News Item',
                'verbose_name_plural': 'News Items',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Nonstop',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField()),
                ('start', models.TimeField(verbose_name='Start')),
                ('end', models.TimeField(verbose_name='End')),
                ('text', ckeditor.fields.RichTextField(null=True, verbose_name='Description', blank=True)),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Nonstop zone',
                'verbose_name_plural': 'Nonstop zones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('datetime', models.DateTimeField()),
                (
                    'weeks',
                    models.IntegerField(
                        default=15,
                        verbose_name='Weeks',
                        choices=[
                            (15, 'Every week'),
                            (1, 'First week'),
                            (2, 'Second week'),
                            (4, 'Third week'),
                            (8, 'Fourth week'),
                            (5, 'First and third week'),
                            (10, 'Second and fourth week'),
                        ],
                    ),
                ),
                ('rerun', models.BooleanField(default=False, verbose_name='Rerun')),
                (
                    'duration',
                    models.IntegerField(
                        help_text='In minutes', null=True, verbose_name='Duration', blank=True
                    ),
                ),
                (
                    'emission',
                    models.ForeignKey(
                        verbose_name='Emission', to='emissions.Emission', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ['datetime'],
                'verbose_name': 'Schedule',
                'verbose_name_plural': 'Schedules',
            },
            bases=(models.Model, emissions.models.WeekdayMixin),
        ),
        migrations.CreateModel(
            name='SoundFile',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'file',
                    models.FileField(
                        upload_to=emissions.models.get_sound_path, max_length=250, verbose_name='File'
                    ),
                ),
                (
                    'podcastable',
                    models.BooleanField(
                        default=False,
                        help_text='The file can be published online according to SABAM rules.',
                        db_index=True,
                        verbose_name='Podcastable',
                    ),
                ),
                (
                    'fragment',
                    models.BooleanField(
                        default=False,
                        help_text='The file is some segment or extra content, not the complete recording.',
                        db_index=True,
                        verbose_name='Fragment',
                    ),
                ),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('duration', models.IntegerField(help_text='In seconds', null=True, verbose_name='Duration')),
                ('got_focus', models.DateTimeField(default=None, null=True, blank=True)),
                ('has_focus', models.BooleanField(default=False)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                (
                    'episode',
                    models.ForeignKey(
                        verbose_name='Episode', to='emissions.Episode', on_delete=models.CASCADE
                    ),
                ),
                (
                    'format',
                    models.ForeignKey(
                        verbose_name='Format',
                        blank=True,
                        to='emissions.Format',
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
            ],
            options={
                'verbose_name': 'Sound file',
                'verbose_name_plural': 'Sound files',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='focus',
            name='newsitem',
            field=models.ForeignKey(
                verbose_name='News Item',
                blank=True,
                to='emissions.NewsItem',
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='focus',
            name='soundfile',
            field=models.ForeignKey(
                verbose_name='Sound file',
                blank=True,
                to='emissions.SoundFile',
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='diffusion',
            name='episode',
            field=models.ForeignKey(verbose_name='Episode', to='emissions.Episode', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='absence',
            name='emission',
            field=models.ForeignKey(
                verbose_name='Emission', to='emissions.Emission', on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
    ]
