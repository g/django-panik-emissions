from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='soundfile',
            options={
                'ordering': ['creation_timestamp'],
                'verbose_name': 'Sound file',
                'verbose_name_plural': 'Sound files',
            },
        ),
    ]
