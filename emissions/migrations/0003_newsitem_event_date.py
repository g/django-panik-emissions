from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0002_auto_20150406_1031'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsitem',
            name='event_date',
            field=models.DateField(null=True, verbose_name='Event Date', blank=True),
            preserve_default=True,
        ),
    ]
