from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0005_auto_20150226_0903'),
        ('emissions', '0003_newsitem_event_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='focus',
            name='page',
            field=models.ForeignKey(
                verbose_name='Page', blank=True, to='data.Page', null=True, on_delete=models.SET_NULL
            ),
            preserve_default=True,
        ),
    ]
