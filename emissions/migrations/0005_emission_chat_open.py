from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0004_focus_page'),
    ]

    operations = [
        migrations.AddField(
            model_name='emission',
            name='chat_open',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
