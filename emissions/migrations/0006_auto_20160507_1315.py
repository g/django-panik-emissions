from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0005_emission_chat_open'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsitem',
            name='date',
            field=models.DateField(
                help_text="The news won't appear on the website before this date.",
                verbose_name='Publication Date',
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='newsitem',
            name='event_date',
            field=models.DateField(
                help_text='If this is an event, set the date here so it appears in the agenda.',
                null=True,
                verbose_name='Event Date',
                blank=True,
            ),
            preserve_default=True,
        ),
    ]
