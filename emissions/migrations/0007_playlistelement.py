from django.db import migrations, models

import emissions.models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0006_auto_20160507_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlaylistElement',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('notes', models.CharField(default=b'', max_length=200, verbose_name='Notes', blank=True)),
                (
                    'sound',
                    models.FileField(
                        upload_to=emissions.models.get_playlist_sound_path,
                        max_length=250,
                        verbose_name='Sound',
                    ),
                ),
                ('order', models.PositiveIntegerField()),
                ('episode', models.ForeignKey(to='emissions.Episode', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': 'Playlist Element',
                'verbose_name_plural': 'Playlist Elements',
            },
            bases=(models.Model,),
        ),
    ]
