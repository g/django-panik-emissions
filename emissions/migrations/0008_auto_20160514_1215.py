from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0007_playlistelement'),
    ]

    operations = [
        migrations.AddField(
            model_name='episode',
            name='effective_end',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='episode',
            name='effective_start',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
