from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0008_auto_20160514_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emission',
            name='categories',
            field=models.ManyToManyField(to='emissions.Category', verbose_name='Categories', blank=True),
        ),
        migrations.AlterField(
            model_name='emission',
            name='colours',
            field=models.ManyToManyField(to='emissions.Colour', verbose_name='Colours', blank=True),
        ),
    ]
