from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0009_auto_20170418_1525'),
    ]

    operations = [
        migrations.AddField(
            model_name='emission',
            name='default_license',
            field=models.CharField(
                default=b'',
                max_length=20,
                verbose_name='Default license for podcasts',
                blank=True,
                choices=[
                    (b'', 'Unspecified'),
                    (b'cc by', 'Creative Commons Attribution'),
                    (b'cc by-sa', 'Creative Commons Attribution ShareAlike'),
                    (b'cc by-nc', 'Creative Commons Attribution NonCommercial'),
                    (b'cc by-nd', 'Creative Commons Attribution NoDerivs'),
                    (b'cc by-nc-sa', 'Creative Commons Attribution NonCommercial ShareAlike'),
                    (b'cc by-nc-nd', 'Creative Commons Attribution NonCommercial NoDerivs'),
                    (b'cc0 / pd', 'Creative Commons Zero / Public Domain'),
                    (b'artlibre', 'Art Libre'),
                ],
            ),
        ),
        migrations.AddField(
            model_name='soundfile',
            name='license',
            field=models.CharField(
                default=b'',
                max_length=20,
                verbose_name='License',
                blank=True,
                choices=[
                    (b'', 'Unspecified'),
                    (b'cc by', 'Creative Commons Attribution'),
                    (b'cc by-sa', 'Creative Commons Attribution ShareAlike'),
                    (b'cc by-nc', 'Creative Commons Attribution NonCommercial'),
                    (b'cc by-nd', 'Creative Commons Attribution NoDerivs'),
                    (b'cc by-nc-sa', 'Creative Commons Attribution NonCommercial ShareAlike'),
                    (b'cc by-nc-nd', 'Creative Commons Attribution NonCommercial NoDerivs'),
                    (b'cc0 / pd', 'Creative Commons Zero / Public Domain'),
                    (b'artlibre', 'Art Libre'),
                ],
            ),
        ),
    ]
