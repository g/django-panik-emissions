from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0010_auto_20170423_1441'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emission',
            name='chat_open',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='episode',
            name='effective_end',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='episode',
            name='effective_start',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
