from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0011_auto_20171002_2231'),
    ]

    operations = [
        migrations.AddField(
            model_name='nonstop',
            name='redirect_path',
            field=models.CharField(max_length=200, verbose_name='Redirect Path', blank=True),
        ),
    ]
