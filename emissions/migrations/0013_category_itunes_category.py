# Generated by Django 1.11.18 on 2019-09-08 21:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0012_nonstop_redirect_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='itunes_category',
            field=models.CharField(
                max_length=100, null=True, verbose_name='iTunes Category Name', blank=True
            ),
        ),
    ]
