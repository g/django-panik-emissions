# Generated by Django 3.2.19 on 2023-09-14 20:14

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0033_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picture',
            name='alt_text',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Alternative Text'),
        ),
        migrations.AlterField(
            model_name='picture',
            name='title',
            field=models.CharField(blank=True, max_length=150, null=True, verbose_name='Title'),
        ),
    ]
