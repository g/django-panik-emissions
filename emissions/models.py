import datetime
import json
import os
import statistics
import urllib.parse

from ckeditor.fields import RichTextField
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import URLValidator
from django.db import models
from django.db.models import Q
from django.db.models.expressions import RawSQL
from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch.dispatcher import receiver
from django.forms import fields
from django.urls import reverse
from django.utils.text import slugify
from django.utils.timezone import is_aware, make_naive
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy
from taggit.managers import TaggableManager

from .app_settings import app_settings
from .utils import get_duration, maybe_resize

LICENSES = (
    ('', _('Unspecified')),
    ('cc by', _('Creative Commons Attribution')),
    ('cc by-sa', _('Creative Commons Attribution ShareAlike')),
    ('cc by-nc', _('Creative Commons Attribution NonCommercial')),
    ('cc by-nd', _('Creative Commons Attribution NoDerivs')),
    ('cc by-nc-sa', _('Creative Commons Attribution NonCommercial ShareAlike')),
    ('cc by-nc-nd', _('Creative Commons Attribution NonCommercial NoDerivs')),
    ('cc0 / pd', _('Creative Commons Zero / Public Domain')),
    ('artlibre', _('Art Libre')),
)

PODCAST_SOUND_QUALITY_LIST = (
    ('standard', _('Standard')),
    ('high', _('High')),
    ('highest', _('Highest')),
)


def get_first_url_from_multi(value):
    if not value:
        return None
    return value.splitlines()[0]


def get_url_kind(url):
    netloc = urllib.parse.urlparse(url).netloc
    if netloc in ('facebook.com', 'www.facebook.com'):
        return 'facebook'
    if netloc in ('t.co', 'twitter.com'):
        return 'twitter'
    if netloc in ('instagram.com', 'www.instagram.com'):
        return 'instagram'
    if netloc in ('twitch.com', 'twitch.tv', 'www.twitch.com', 'www.twitch.tv'):
        return 'twitch'
    if netloc == 'www.youtube.com':
        return 'youtube'
    if netloc.endswith('.bandcamp.com'):
        return 'bandcamp'
    if netloc in ('soundcloud.com', 'on.soundcloud.com'):
        return 'soundcloud'
    if netloc == 'www.mixcloud.com':
        return 'mixcloud'
    return None


def get_urls_and_kind(value):
    for url in (value or '').splitlines():
        yield (get_url_kind(url), url)


def get_urls_by_kind(value):
    urls = []
    for url in (value or '').splitlines():
        kind = get_url_kind(url) or '_website'
        urls.append((kind, url))
    urls.sort()
    urls = [(x.strip('_'), y) for x, y in urls]
    return urls


def generate_slug(instance, **query_filters):
    base_slug = instance.base_slug
    slug = base_slug
    i = 1

    # no optimization: check slug in DB each time
    while instance._meta.model.objects.filter(slug=slug, **query_filters).exists():
        slug = '%s-%s' % (base_slug, i)
        i += 1
    return slug


class WeekdayMixin:
    def get_weekday(self):
        weekday = self.datetime.weekday() + 7
        if self.datetime.time() < datetime.time(app_settings.DAY_HOUR_START, app_settings.DAY_MINUTE_START):
            weekday -= 1
        weekday %= 7
        return weekday

    def is_on_weekday(self, day):  # day is [1..7]
        week_day = self.datetime.weekday()
        if (self.datetime.hour, self.datetime.minute) <= (
            app_settings.DAY_HOUR_START,
            app_settings.DAY_MINUTE_START,
        ):
            week_day -= 1
        week_day = (week_day % 7) + 1
        if hasattr(self, 'episode'):
            if (self.datetime.hour, self.datetime.minute) <= (
                app_settings.DAY_HOUR_START,
                app_settings.DAY_MINUTE_START,
            ) and (
                self.end_datetime.hour,
                self.end_datetime.minute,
            ) > (
                app_settings.DAY_HOUR_START,
                app_settings.DAY_MINUTE_START,
            ):
                if (self.end_datetime.weekday() + 1) == day:
                    return True
        return week_day == day


class CategoryManager(models.Manager):
    def get_by_natural_key(self, key):
        try:
            return self.get(slug=key)
        except ObjectDoesNotExist:
            return self.get(id=key)


class Category(models.Model):
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['title']

    title = models.CharField(_('Title'), max_length=50)
    slug = models.SlugField(null=True)
    itunes_category = models.CharField(_('iTunes Category Name'), max_length=100, null=True, blank=True)
    archived = models.BooleanField(pgettext_lazy('category', 'Archived'), default=False)

    objects = CategoryManager()

    def natural_key(self):
        return (self.slug,) if self.slug else (self.id,)

    def sorted_emission(self):
        return self.emission_set.order_by('title')

    def __str__(self):
        return self.title


class Format(models.Model):
    class Meta:
        verbose_name = _('Format')
        verbose_name_plural = _('Formats')
        ordering = ['title']

    title = models.CharField(_('Title'), max_length=50)
    slug = models.SlugField(null=True)
    archived = models.BooleanField(pgettext_lazy('format', 'Archived'), default=False)

    def __str__(self):
        return self.title


def get_image_path(instance, filename):
    if isinstance(instance, Emission):
        return os.path.join('images', instance.slug, os.path.basename(filename))
    if isinstance(instance, Nonstop):
        return os.path.join('images', 'nonstop', instance.slug, os.path.basename(filename))
    if isinstance(instance, Episode):
        return os.path.join('images', instance.emission.slug, os.path.basename(filename))
    if isinstance(instance, NewsItem):
        return os.path.join('images', 'news', instance.slug, os.path.basename(filename))
    if isinstance(instance, Picture):
        return get_image_path(instance.get_content_object(), filename)


class MultiURLField(models.TextField):
    def validate(self, value, model_instance):
        super().validate(value, model_instance)
        url_validator = URLValidator(schemes=('http', 'https'))
        for line in (value or '').splitlines():
            url_validator(line)


class Emission(models.Model):
    class Meta:
        verbose_name = _('Emission')
        verbose_name_plural = _('Emissions')
        ordering = ['title']

    title = models.CharField(_('Title'), max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    subtitle = models.CharField(_('Subtitle'), max_length=80, null=True, blank=True)
    text = RichTextField(_('Description'), null=True)
    archived = models.BooleanField(pgettext_lazy('emission', 'Archived'), default=False)
    categories = models.ManyToManyField(Category, verbose_name=_('Categories'), blank=True)

    # XXX: languages (models.ManyToManyField(Language))

    duration = models.IntegerField(_('Duration'), default=60, help_text=_('In minutes'))

    default_license = models.CharField(
        _('Default license for podcasts'), max_length=20, blank=True, default='', choices=LICENSES
    )
    podcast_sound_quality = models.CharField(
        _('Podcast sound quality'), max_length=20, default='standard', choices=PODCAST_SOUND_QUALITY_LIST
    )
    email = models.EmailField(_('Email'), max_length=254, null=True, blank=True)
    website = MultiURLField(_('Website'), null=True, blank=True)

    image_usage_ok = models.BooleanField(_('Include image'), default=False)
    image = models.ImageField(_('Image'), upload_to=get_image_path, max_length=250, null=True, blank=True)
    image_attribution_text = models.CharField(
        _('Text for image attribution'), max_length=250, null=True, blank=True
    )
    image_attribution_url = models.URLField(_('URL for image attribution'), null=True, blank=True)
    tags = TaggableManager(_('Tags'), blank=True)

    chat_open = models.DateTimeField(null=True, blank=True)

    # denormalized from Focus
    got_focus = models.DateTimeField(default=None, null=True, blank=True)
    has_focus = models.BooleanField(default=False)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    def get_absolute_url(self):
        return reverse('emission-view', kwargs={'slug': str(self.slug)})

    def __str__(self):
        return self.title

    @property
    def base_slug(self):
        return slugify(self.title).strip('-') or 'untitled'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)
        if self.id is not None and self.image:
            maybe_resize(self.image.path)

    def get_recurring_content(self):
        from nonstop.models import RecurringPlaylistDiffusion, RecurringStreamDiffusion

        recurring_stream = RecurringStreamDiffusion.objects.filter(schedule__in=self.schedule_set.all())
        if recurring_stream.exists():
            return recurring_stream
        recurring_playlist = RecurringPlaylistDiffusion.objects.filter(schedule__in=self.schedule_set.all())
        if recurring_playlist.exists():
            return recurring_playlist

    def get_week_compacted_schedules(self, rerun=None):
        schedules = [
            x
            for x in self.schedule_set.all().order_by('datetime')
            if x.is_current() and ((rerun is None and True) or x.rerun is rerun)
        ]
        if len(schedules) > 1 and len({x.datetime for x in schedules}) == 1:
            # multiple schedules for same day/hour, must be a custom week combination,
            # alter first schedule week bits
            for schedule in schedules[1:]:
                schedules[0].weeks |= schedule.weeks
            return [schedules[0]]
        return schedules

    def get_compacted_schedules(self, rerun=None):
        schedules = self.get_week_compacted_schedules(rerun=rerun)
        if (
            len(schedules) > 1
            and len({x.datetime.time() for x in schedules}) == 1
            and len({x.weeks for x in schedules}) == 1
        ):
            # multiple schedules for same hour, join consecutive weekdays
            sequences = [[schedules[0].datetime]]
            for schedule in schedules[1:]:
                if schedule.datetime == sequences[-1][-1] + datetime.timedelta(days=1):
                    sequences[-1].append(schedule.datetime)
                else:
                    sequences.append([schedule.datetime])
            for i, sequence in enumerate(sequences):
                # alter existing schedules
                schedules[i].multiple_days = bool(len(sequence) > 1)
                schedules[i].first_weekday_datetime = sequence[0]
                schedules[i].last_weekday_datetime = sequence[-1]
                yield schedules[i]
        else:
            yield from schedules

    def get_schedules(self):
        return list(self.get_compacted_schedules())

    def get_schedules_no_reruns(self):
        return list(self.get_compacted_schedules(rerun=False))

    def get_schedules_reruns(self):
        return list(self.get_compacted_schedules(rerun=True))

    def get_sorted_episodes(self):
        return (
            self.episode_set.select_related()
            .annotate(
                latest_soundfile_timestamp=RawSQL(
                    '''SELECT MAX(emissions_soundfile.creation_timestamp)
                         FROM emissions_soundfile
                        WHERE emissions_soundfile.episode_id = emissions_episode.id''',
                    (),
                )
            )
            .exclude(first_diffusion_date__isnull=True)
            .order_by('-first_diffusion_date')
        )

    def get_sorted_newsitems(self):
        return self.newsitem_set.select_related().order_by('-date')

    def get_next_planned_date_and_schedule(self, since=None, include_rerun=False):
        if include_rerun:
            schedules = self.schedule_set.all()
        else:
            schedules = self.schedule_set.filter(rerun=False)
        schedules = [x for x in schedules if x.is_current()]
        if not schedules:
            return None
        if since is None:
            since = datetime.datetime.today()
        possible_dates = []
        for schedule in schedules:
            possible_dates.append((schedule.get_next_planned_date(since), schedule))
        possible_dates.sort(key=lambda x: x[0])
        return possible_dates[0]

    def get_next_planned_date(self, since=None, include_rerun=False):
        result = self.get_next_planned_date_and_schedule(since=since, include_rerun=include_rerun)
        return result[0] if result else None

    def get_next_planned_duration(self, since=None, include_rerun=False):
        result = self.get_next_planned_date_and_schedule(since=since, include_rerun=include_rerun)
        if not result:
            return None
        return result[1].duration or self.duration

    def get_website_url(self):
        return get_first_url_from_multi(self.website)

    def get_website_urls(self):
        return get_urls_by_kind(self.website)

    def get_site_url(self):
        return urllib.parse.urljoin(
            settings.WEBSITE_BASE_URL, os.path.join(settings.WEBSITE_EMISSIONS_PREFIX, self.slug, '')
        )


class Schedule(models.Model, WeekdayMixin):
    class Meta:
        verbose_name = _('Schedule')
        verbose_name_plural = _('Schedules')
        ordering = ['datetime']

    WEEK_CHOICES = (
        (0b01111, _('Every week')),
        (0b00001, _('First week')),
        (0b00010, _('Second week')),
        (0b00100, _('Third week')),
        (0b01000, _('Fourth week')),
        (0b00101, _('First and third week')),
        (0b01010, _('Second and fourth week')),
        (0b10000, _('Every other week')),
        (0b10001, _('Every other week (alt)')),
    )
    emission = models.ForeignKey('Emission', verbose_name='Emission', on_delete=models.CASCADE)
    datetime = models.DateTimeField(_('Day/time'))
    weeks = models.IntegerField(_('Weeks'), default=15, choices=WEEK_CHOICES)
    rerun = models.BooleanField(_('Rerun'), default=False)
    duration = models.IntegerField(_('Duration'), null=True, blank=True, help_text=_('In minutes'))
    start_date = models.DateField(_('Start date'), null=True, blank=True)
    end_date = models.DateField(_('End date'), null=True, blank=True)
    info_only_schedule = models.BooleanField(
        verbose_name=_('Only use schedule for information (no airplay)'), default=False
    )

    @property
    def weeks_string(self):
        week_ordinals = []
        if self.weeks in (0b10000, 0b10001):
            return _('Every other week')
        if self.weeks & 0b0001:
            week_ordinals.append(gettext('1st'))
        if self.weeks & 0b0010:
            week_ordinals.append(gettext('2nd'))
        if self.weeks & 0b0100:
            week_ordinals.append(gettext('3rd'))
        if self.weeks & 0b1000:
            week_ordinals.append(gettext('4th'))
        if not week_ordinals or len(week_ordinals) == 4:
            return
        if len(week_ordinals) == 1:
            return gettext('%s of the month') % week_ordinals[0]
        if len(week_ordinals) == 2:
            return gettext(' and ').join(week_ordinals)
        return gettext(' and ').join([', '.join(week_ordinals[:-1]), week_ordinals[-1]])

    def week_sort_key(self):
        order = [
            0b01111,  # Every week
            0b00001,  # First week
            0b00101,  # First and third week
            0b00010,  # Second week
            0b01010,  # Second and fourth week
            0b00100,  # Third week
            0b01000,  # Fourth week
            0b10000,  # Every other week
            0b10001,  # Every other week (alt)
        ]
        if self.weeks in order:
            return order.index(self.weeks)
        return -1

    def get_duration(self):
        if self.duration:
            return self.duration
        else:
            return self.emission.duration

    def is_past(self, date=None):
        return bool(self.end_date and (date or datetime.date.today()) > self.end_date)

    def is_future(self, date=None):
        return bool(self.start_date and (date or datetime.date.today()) < self.start_date)

    def is_current(self, date=None):
        return not (self.is_past(date) or self.is_future(date))

    @property
    def end_datetime(self):
        return self.datetime + datetime.timedelta(minutes=self.get_duration())

    def match_week(self, week_no, current_date=None):
        if self.weeks in (0b10000, 0b10001):
            # every other week
            # 2006-01-01 is a Sunday, this allows the calculation below to have
            # week/other week switch on Monday.
            reference_date = datetime.date(2006, 1, 1)
            diff_days = (reference_date - current_date.date()).days
            if self.weeks == 0b10000:
                return bool(diff_days % 14 >= 7)
            else:
                return bool(diff_days % 14 < 7)

        if week_no == 4:
            # this is the fifth week of the month, only return True for
            # emissions scheduled every week.
            return self.weeks == 0b1111

        if self.weeks & (0b0001 << (week_no)) == 0:
            return False

        return True

    def matches(self, dt):
        weekday = dt.weekday()
        if (dt.hour, dt.minute) < (app_settings.DAY_HOUR_START, app_settings.DAY_MINUTE_START):
            weekday -= 1
        if weekday != self.get_weekday():
            return False
        elif self.weeks in (0b10000, 0b10001):  # every other week
            if not self.match_week(None, dt):
                return False
        elif self.weeks != 0b1111:
            week_no = (dt.day - 1) // 7
            if self.match_week(week_no) is False:
                return False
        if not isinstance(dt, datetime.datetime):
            # simple datetime.date, -> ignore time
            return True
        start_time = self.datetime.time()
        end_time = (self.datetime + datetime.timedelta(minutes=self.get_duration())).time()
        if (start_time < end_time and start_time <= dt.time() < end_time) or (
            start_time > end_time and (dt.time() >= start_time or dt.time() < end_time)
        ):
            return True
        return False

    def get_next_planned_date(self, since):
        if since is None:
            since = datetime.datetime.today()
        possible_dates = []
        monday_since_date = since - datetime.timedelta(days=since.weekday())
        if is_aware(self.datetime):
            # required for sqlite tests
            self.datetime = make_naive(self.datetime)
        start_week_date = self.datetime.replace(
            year=monday_since_date.year, month=monday_since_date.month, day=monday_since_date.day
        )
        start_week_date -= datetime.timedelta(days=start_week_date.weekday())
        start_week_date += datetime.timedelta(days=self.datetime.weekday())
        for i in range(6):
            week_date = start_week_date + datetime.timedelta(days=i * 7)
            if week_date < since:
                continue
            if self.match_week((week_date.day - 1) // 7, week_date):
                possible_dates.append(week_date)
        possible_dates.sort()
        if self.emission.absence_set.filter(datetime=possible_dates[0]).exists():
            return self.get_next_planned_date(since=possible_dates[0] + datetime.timedelta(minutes=10))
        return possible_dates[0]

    def __str__(self):
        return '%s at %s' % (self.emission.title, self.datetime.strftime('%a %H:%M'))


class Episode(models.Model):
    class Meta:
        verbose_name = _('Episode')
        verbose_name_plural = _('Episodes')
        ordering = ['title']

    emission = models.ForeignKey('Emission', verbose_name=_('Emission'), on_delete=models.CASCADE)
    title = models.CharField(_('Title'), max_length=200)
    slug = models.SlugField(max_length=200)
    subtitle = models.CharField(_('Subtitle'), max_length=150, null=True, blank=True)
    text = RichTextField(_('Description'), null=True)
    extra_links = MultiURLField(_('Extra links'), null=True, blank=True)
    tags = TaggableManager(_('Tags'), blank=True)
    duration = models.IntegerField(_('Duration'), null=True, blank=True, help_text=_('In minutes'))

    image_usage_ok = models.BooleanField(_('Include image'), default=False)
    image = models.ImageField(_('Image'), upload_to=get_image_path, max_length=250, null=True, blank=True)
    image_attribution_text = models.CharField(
        _('Text for image attribution'), max_length=250, null=True, blank=True
    )
    image_attribution_url = models.URLField(_('URL for image attribution'), null=True, blank=True)

    agenda_only = models.BooleanField(_('Only include in agenda'), default=False)

    effective_start = models.DateTimeField(null=True, blank=True)
    effective_end = models.DateTimeField(null=True, blank=True)

    # denormalized from Focus
    got_focus = models.DateTimeField(default=None, null=True, blank=True)
    has_focus = models.BooleanField(default=False)
    # denormalized from Diffusion
    first_diffusion_date = models.DateTimeField(db_index=True, null=True)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    # XXX: languages (models.ManyToManyField(Language))

    def __str__(self):
        return self.title

    @property
    def base_slug(self):
        return slugify(self.title).strip('-') or 'untitled'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)
        if self.id is not None and self.image:
            maybe_resize(self.image.path)

    def cache_first_diffusion_date(self):
        current_value = self.first_diffusion_date
        try:
            self.first_diffusion_date = self.diffusions()[0].datetime
        except IndexError:
            self.first_diffusion_date = None
        if self.first_diffusion_date != current_value:
            self.save(update_fields=['first_diffusion_date'])

    def get_duration(self):
        if self.duration:
            return self.duration
        else:
            return self.emission.duration

    def get_absolute_url(self):
        return reverse(
            'episode-view', kwargs={'emission_slug': str(self.emission.slug), 'slug': str(self.slug)}
        )

    def get_site_url(self):
        return urllib.parse.urljoin(
            settings.WEBSITE_BASE_URL,
            os.path.join(settings.WEBSITE_EMISSIONS_PREFIX, self.emission.slug, self.slug, ''),
        )

    def has_sound(self):
        if hasattr(self, 'latest_soundfile_timestamp'):
            return bool(self.latest_soundfile_timestamp)
        return self.soundfile_set.count() > 0

    def get_pige_download_url(self):
        return '%s/%s-%s-%s.wav' % (
            settings.PIGE_DOWNLOAD_BASE_URL,
            self.effective_start.strftime('%Y%m%d'),
            self.effective_start.strftime('%Hh%Mm%S.%f'),
            self.effective_end.strftime('%Hh%Mm%S.%f'),
        )

    _soundfiles = {}

    @classmethod
    def set_prefetched_soundfiles(cls, soundfiles):
        cls._soundfiles.update(soundfiles)

    def has_prefetched_soundfile(self):
        return self.id in self._soundfiles

    def get_prefetched_soundfile(self):
        return self._soundfiles.get(self.id)

    _main_sound = False

    @property
    def main_sound(self):
        if self._main_sound is not False:
            return self._main_sound

        if self.has_prefetched_soundfile():
            self._main_sound = self.get_prefetched_soundfile()
            return self._main_sound

        t = self.soundfile_set.published().exclude(fragment=True)
        if t:
            self._main_sound = t[0]
        else:
            self._main_sound = None

        return self._main_sound

    @main_sound.setter
    def main_sound(self, value):
        self._main_sound = value

    def podcastable_sounds(self):
        return self.soundfile_set.exclude(podcastable=False)

    def fragment_sounds(self):
        return self.soundfile_set.exclude(podcastable=False).exclude(fragment=False)

    def main_sounds(self):
        return self.soundfile_set.exclude(fragment=True)

    def diffusions(self):
        return Diffusion.objects.filter(episode=self.id).order_by('datetime')

    def get_extra_links_and_kind(self):
        yield from get_urls_and_kind(self.extra_links)

    @property
    def first_diffusion(self):  # backward compatibility
        return getattr(self, '_first_diffusion', self.first_diffusion_date)

    @first_diffusion.setter
    def first_diffusion(self, value):
        self._first_diffusion = value


class Diffusion(models.Model, WeekdayMixin):
    class Meta:
        verbose_name = _('Diffusion')
        verbose_name_plural = _('Diffusions')
        ordering = ['datetime']

    episode = models.ForeignKey('Episode', verbose_name=_('Episode'), on_delete=models.CASCADE)
    datetime = models.DateTimeField(verbose_name=_('Date/time'), db_index=True)
    info_only_schedule = models.BooleanField(
        verbose_name=_('Only use schedule for information (no airplay)'), default=False
    )

    def __str__(self):
        return '%s at %02d:%02d' % (self.episode.title, self.datetime.hour, self.datetime.minute)

    def get_duration(self):
        return self.episode.get_duration()

    @property
    def end_datetime(self):
        return self.datetime + datetime.timedelta(minutes=self.get_duration())

    @property
    def emission(self):
        return self.episode.emission


class Absence(models.Model):
    class Meta:
        verbose_name = _('Absence')
        verbose_name_plural = _('Absences')

    emission = models.ForeignKey('Emission', verbose_name='Emission', on_delete=models.CASCADE)
    datetime = models.DateTimeField(_('Date/time'), db_index=True)

    def __str__(self):
        return 'Absence for %s on %s' % (self.emission.title, self.datetime)


def get_sound_path(instance, filename):
    return os.path.join('sounds.orig', instance.episode.emission.slug, os.path.basename(filename))


class SoundFileManager(models.Manager):
    def published(self):
        # this gets podcasts online one hour after the start of the first diffusion,
        # it doesn't consider the real durations of the episodes but it's fine.
        dt = datetime.datetime.now() - datetime.timedelta(minutes=60)
        return self.filter(podcastable=True).filter(
            Q(wait_after_diffusion=False) | Q(episode__first_diffusion_date__lt=dt)
        )


class SoundFile(models.Model):
    objects = SoundFileManager()

    class Meta:
        verbose_name = _('Sound file')
        verbose_name_plural = _('Sound files')
        ordering = ['order', 'creation_timestamp']

    episode = models.ForeignKey('Episode', verbose_name=_('Episode'), on_delete=models.CASCADE)
    file = models.FileField(_('File'), upload_to=get_sound_path, max_length=250, blank=True, null=True)
    external_url = models.URLField(_('URL'), null=True, blank=True)
    podcastable = models.BooleanField(
        _('Published'),
        default=True,
        db_index=True,
        help_text=_('Get the sound published on the website'),
    )
    wait_after_diffusion = models.BooleanField(
        _('Wait after the episode has been broadcast'),
        default=True,
    )
    fragment = models.BooleanField(
        _('Fragment'),
        default=False,
        db_index=True,
        help_text=_('The file is some segment or extra content, not the complete recording.'),
    )
    title = models.CharField(_('Title'), max_length=200)
    duration = models.IntegerField(_('Duration'), null=True, help_text=_('In seconds'))
    mp3_file_size = models.IntegerField(null=True)  # used in rss feeds

    format = models.ForeignKey(
        'Format', verbose_name=_('Format'), null=True, blank=True, on_delete=models.SET_NULL
    )
    license = models.CharField(_('License'), max_length=20, blank=True, default='', choices=LICENSES)

    order = models.PositiveIntegerField(default=0)

    # denormalized from Focus
    got_focus = models.DateTimeField(default=None, null=True, blank=True)
    has_focus = models.BooleanField(default=False)

    # basic statistics
    download_count = models.IntegerField(_('Download Count'), default=0)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    def compute_duration(self):
        if not self.file:
            return
        for path in (self.get_format_path('ogg'), self.get_format_path('mp3'), self.file.path):
            self.duration = get_duration(path)
            if self.duration:
                return

    def get_url(self):
        if self.file:
            return self.file.url
        if self.external_url:
            return self.external_url

    def get_external_host(self):
        if not self.external_url:
            return
        parts = urllib.parse.urlparse(self.external_url)
        if parts.netloc == 'www.mixcloud.com':
            return 'mixcloud'
        if parts.netloc == 'api.soundcloud.com':
            return 'soundcloud'

    def get_external_embed_url(self):
        if not self.external_url:
            return
        parts = urllib.parse.urlparse(self.external_url)
        if parts.netloc == 'www.mixcloud.com':
            return 'https://www.mixcloud.com/widget/iframe/?feed=%s' % urllib.parse.quote_plus(parts.path)
        if parts.netloc == 'api.soundcloud.com':
            return 'https://w.soundcloud.com/player/?url=%s' % urllib.parse.quote_plus(self.external_url)

    def get_format_filename(self, format):
        return '%s_%05d__%s.%s' % (self.episode.slug, self.id, (self.fragment and '0' or '1'), format)

    def get_format_path(self, format):
        if not self.file:
            return None
        return '%s/%s' % (
            os.path.dirname(self.file.path).replace('.orig', ''),
            self.get_format_filename(format),
        )

    def get_format_url(self, format):
        if not self.file:
            return None
        return '%s/%s' % (
            os.path.dirname(self.file.url).replace('.orig', ''),
            self.get_format_filename(format),
        )

    def get_duration_string(self):
        if not self.duration:
            return ''
        return '%d:%02d' % (self.duration / 60, self.duration % 60)

    def get_durations(self):
        durations = [self.episode.emission.duration * 60, self.episode.get_duration() * 60] + [
            x.get_duration() * 60 for x in self.episode.diffusion_set.all()
        ]
        return durations

    def is_too_long(self):
        if self.fragment or self.external_url:
            return False
        return bool(self.duration > max(self.get_durations()) * 1.2)

    def is_too_short(self):
        if self.fragment or self.external_url:
            return False
        return bool(self.duration < min(self.get_durations()) * 0.5)

    def has_low_volume(self):
        waveform_json = self.get_format_path('waveform.json')
        if waveform_json and os.path.exists(waveform_json):
            with open(waveform_json) as wavefile_fd:
                median_volume = statistics.median(json.load(wavefile_fd))
                return bool(median_volume < 10)
        return False

    def __str__(self):
        return '%s - %s' % (self.title or self.id, self.episode.title)

    @property
    def first_diffusion(self):  # backward compatibility
        return getattr(self, '_first_diffusion', self.episode.first_diffusion_date)

    @first_diffusion.setter
    def first_diffusion(self, value):
        self._first_diffusion = value


class NewsCategoryManager(models.Manager):
    def get_by_natural_key(self, key):
        try:
            return self.get(slug=key)
        except ObjectDoesNotExist:
            return self.get(id=key)


class NewsCategory(models.Model):
    class Meta:
        verbose_name = _('News Category')
        verbose_name_plural = _('News Categories')
        ordering = ['title']

    title = models.CharField(_('Title'), max_length=50)
    slug = models.SlugField(null=True)
    archived = models.BooleanField(pgettext_lazy('category', 'Archived'), default=False)

    objects = NewsCategoryManager()

    def natural_key(self):
        return (self.slug,) if self.slug else (self.id,)

    def __str__(self):
        return self.title

    def get_sorted_newsitems(self):
        return self.newsitem_set.select_related().order_by('-date')


class NewsItem(models.Model):
    class Meta:
        verbose_name = _('News Item')
        verbose_name_plural = _('News Items')
        ordering = ['title']

    title = models.CharField(_('Title'), max_length=200)
    subtitle = models.CharField(_('Subtitle'), max_length=80, null=True, blank=True)
    slug = models.SlugField(max_length=200)
    text = RichTextField(_('Description'))
    date = models.DateField(
        _('Publication Date'), help_text=_('The news won\'t appear on the website before this date.')
    )

    image_usage_ok = models.BooleanField(_('Include image'), default=False)
    image = models.ImageField(_('Image'), upload_to=get_image_path, max_length=250, null=True, blank=True)
    image_attribution_text = models.CharField(
        _('Text for image attribution'), max_length=250, null=True, blank=True
    )
    image_attribution_url = models.URLField(_('URL for image attribution'), null=True, blank=True)

    tags = TaggableManager(_('Tags'), blank=True)
    category = models.ForeignKey(
        'NewsCategory', verbose_name=_('Category'), null=True, blank=True, on_delete=models.SET_NULL
    )
    emission = models.ForeignKey(
        'Emission', verbose_name=_('Emission'), null=True, blank=True, on_delete=models.CASCADE
    )

    expiration_date = models.DateField(_('Expiration Date'), null=True, blank=True)
    event_date = models.DateField(
        _('Event Date'),
        null=True,
        blank=True,
        help_text=_('If this is an event, set the date here so it appears in the agenda.'),
    )

    # denormalized from Focus
    got_focus = models.DateTimeField(default=None, null=True, blank=True)
    has_focus = models.BooleanField(default=False)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.title

    @property
    def base_slug(self):
        return slugify(self.title).strip('-') or 'untitled'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('newsitem-view', kwargs={'slug': str(self.slug)})

    def get_site_url(self):
        return urllib.parse.urljoin(
            settings.WEBSITE_BASE_URL, os.path.join(settings.WEBSITE_NEWSITEMS_PREFIX, self.slug)
        )


class Nonstop(models.Model):
    class Meta:
        verbose_name = _('Nonstop zone')
        verbose_name_plural = _('Nonstop zones')
        ordering = ['title']

    title = models.CharField(_('Title'), max_length=50)
    slug = models.SlugField()

    start = models.TimeField(_('Start'))
    end = models.TimeField(_('End'))

    subtitle = models.CharField(_('Subtitle'), max_length=80, null=True, blank=True)
    text = RichTextField(_('Description'), null=True, blank=True)

    image_usage_ok = models.BooleanField(_('Include image'), default=False)
    image = models.ImageField(_('Image'), upload_to=get_image_path, max_length=250, null=True, blank=True)
    image_attribution_text = models.CharField(
        _('Text for image attribution'), max_length=250, null=True, blank=True
    )
    image_attribution_url = models.URLField(_('URL for image attribution'), null=True, blank=True)

    redirect_path = models.CharField(_('Redirect Path'), max_length=200, blank=True)

    def __str__(self):
        return self.title

    @property
    def base_slug(self):
        return slugify(self.title).strip('-') or 'untitled'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)

    def get_public_label(self):
        return self.title.split('(')[0].strip()

    def get_playlist_emissions(self):
        emissions = {}
        for recurring_playlist in self.recurring_playlist_zones.all().select_related(
            'schedule', 'schedule__emission'
        ):
            emissions[recurring_playlist.schedule.emission_id] = recurring_playlist.schedule.emission
        return emissions.values()


class Focus(models.Model):
    title = models.CharField(_('Alternate Title'), max_length=50, null=True, blank=True)
    newsitem = models.ForeignKey(
        'NewsItem', verbose_name=_('News Item'), null=True, blank=True, on_delete=models.CASCADE
    )
    emission = models.ForeignKey(
        'Emission', verbose_name=_('Emission'), null=True, blank=True, on_delete=models.CASCADE
    )
    episode = models.ForeignKey(
        'Episode', verbose_name=_('Episode'), null=True, blank=True, on_delete=models.CASCADE
    )
    soundfile = models.ForeignKey(
        'SoundFile', verbose_name=_('Sound file'), null=True, blank=True, on_delete=models.CASCADE
    )
    page = models.ForeignKey(
        'data.Page', verbose_name=_('Page'), null=True, blank=True, on_delete=models.CASCADE
    )
    current = models.BooleanField('Current', default=True)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        if self.newsitem:
            return 'Newsitem: %s' % self.newsitem.title
        if self.emission:
            return 'Emission: %s' % self.emission.title
        if self.episode:
            return 'Episode: %s' % self.episode.title
        if self.soundfile:
            return 'Soundfile: %s' % (self.soundfile.title or self.soundfile.episode.title)
        if self.page:
            return 'Page: %s' % self.page.title
        return '%s' % self.id

    def focus_title(self):
        if self.title:
            return self.title
        if self.newsitem:
            return self.newsitem.title
        if self.emission:
            return self.emission.title
        if self.page:
            return self.page.title

        episode = None
        if self.soundfile:
            if self.soundfile.fragment and self.soundfile.title:
                return self.soundfile.title
            episode = self.soundfile.episode
        elif self.episode:
            episode = self.episode

        if episode:
            if episode.title:
                return episode.title
            else:
                return episode.emission.title

        return None

    def content_image(self):
        if self.newsitem:
            return self.newsitem.image
        if self.emission:
            return self.emission.image
        if self.page:
            return self.page.picture

        episode = None
        if self.soundfile:
            episode = self.soundfile.episode
        elif self.episode:
            episode = self.episode

        if episode:
            if episode.image:
                return episode.image
            else:
                return episode.emission.image

    def content_category_title(self):
        if self.newsitem:
            if self.newsitem.category:
                return self.newsitem.category.title
            return _('News')
        if self.emission:
            return _('Emission')
        if self.episode:
            return self.episode.emission.title
        if self.soundfile:
            return self.soundfile.episode.emission.title
        if self.page:
            return 'Topik'

    def get_related_object(self):
        try:
            if self.newsitem:
                return self.newsitem
            if self.emission:
                return self.emission
            if self.episode:
                return self.episode
            if self.soundfile:
                return self.soundfile
            if self.page:
                return self.page
        except ObjectDoesNotExist:
            return None

        return None


class Picture(models.Model):
    class Meta:
        ordering = ['order', 'creation_timestamp']

    title = models.CharField(_('Title'), max_length=150, null=True, blank=True)
    alt_text = models.CharField(_('Alternative Text'), max_length=500, null=True, blank=True)

    image_usage_ok = models.BooleanField(_('Include image'), default=False)
    image = models.ImageField(_('Picture'), upload_to=get_image_path, max_length=250, null=False, blank=False)
    image_attribution_text = models.CharField(
        _('Text for picture attribution'), max_length=250, null=True, blank=True
    )
    image_attribution_url = models.URLField(_('URL for picture attribution'), null=True, blank=True)

    newsitem = models.ForeignKey(
        'NewsItem', verbose_name=_('News Item'), null=True, blank=True, on_delete=models.CASCADE
    )
    emission = models.ForeignKey(
        'Emission', verbose_name=_('Emission'), null=True, blank=True, on_delete=models.CASCADE
    )
    episode = models.ForeignKey(
        'Episode', verbose_name=_('Episode'), null=True, blank=True, on_delete=models.CASCADE
    )

    order = models.PositiveIntegerField(default=999)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    def get_content_object(self):
        return self.newsitem or self.emission or self.episode


def get_playlist_sound_path(instance, filename):
    return os.path.join(
        'playlists', instance.episode.emission.slug, instance.episode.slug, os.path.basename(filename)
    )


class PlaylistElement(models.Model):
    episode = models.ForeignKey('Episode', null=True, on_delete=models.CASCADE)
    title = models.CharField(_('Title'), max_length=200)
    notes = models.CharField(_('Notes'), max_length=200, blank=True, default='')
    sound = models.FileField(_('Sound'), upload_to=get_playlist_sound_path, max_length=250)
    order = models.PositiveIntegerField()

    class Meta:
        verbose_name = _('Playlist Element')
        verbose_name_plural = _('Playlist Elements')
        ordering = ['order']

    def shortcut(self):
        return chr(ord('a') + self.order - 1)


@receiver(pre_save, sender=Focus, dispatch_uid='focus_pre_save')
def set_focus_on_save(sender, instance, **kwargs):
    object = instance.get_related_object()
    if not hasattr(object, 'has_focus'):
        return
    must_save = False
    if instance.current != object.has_focus:
        object.has_focus = instance.current
        must_save = True
    if object and not object.got_focus and instance.current:
        object.got_focus = datetime.datetime.now()
        must_save = True
    if must_save:
        object.save()


@receiver(post_delete, sender=Focus, dispatch_uid='focus_post_delete')
def remove_focus_on_delete(sender, instance, **kwargs):
    object = instance.get_related_object()
    if object and (object.got_focus or object.has_focus):
        object.got_focus = None
        object.has_focus = False
        object.save()


@receiver(post_save, sender=Diffusion, dispatch_uid='diffusion_pre_save')
def set_diffusion_on_save(sender, instance, **kwargs):
    instance.episode.cache_first_diffusion_date()


@receiver(post_delete, sender=Diffusion, dispatch_uid='diffusion_post_delete')
def remove_diffusion_on_delete(sender, instance, **kwargs):
    instance.episode.cache_first_diffusion_date()
