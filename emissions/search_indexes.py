import datetime

from haystack import indexes

from .models import Emission, Episode, NewsItem, SoundFile


class EmissionIndex(indexes.SearchIndex, indexes.Indexable):
    title = indexes.CharField(model_attr='title', boost=1.5)
    text = indexes.CharField(document=True, use_template=True)
    categories = indexes.MultiValueField(faceted=True)

    slug = indexes.CharField(model_attr='slug', indexed=False)
    subtitle = indexes.CharField(model_attr='subtitle', indexed=False, null=True)

    def prepare(self, obj):
        data = super().prepare(obj)
        data['boost'] = 1.3
        return data

    def get_model(self):
        return Emission

    def prepare_categories(self, obj):
        return [category.title for category in obj.categories.all()]

    def get_updated_field(self):
        return 'last_update_timestamp'


class EpisodeIndex(indexes.SearchIndex, indexes.Indexable):
    title = indexes.CharField(model_attr='title', boost=1.5)
    text = indexes.CharField(document=True, use_template=True)
    tags = indexes.MultiValueField(faceted=True)
    emission_slug = indexes.CharField(faceted=True)

    has_sound = indexes.BooleanField(indexed=False)
    slug = indexes.CharField(model_attr='slug', indexed=False)
    emission_title = indexes.CharField(indexed=False)

    def get_model(self):
        return Episode

    def prepare_emission_slug(self, obj):
        return obj.emission.slug

    def prepare_emission_title(self, obj):
        return obj.emission.title

    def prepare_tags(self, obj):
        return list({tag.name.lower() for tag in obj.tags.all()})

    def prepare_has_sound(self, obj):
        return obj.soundfile_set.exclude(podcastable=False).count() > 0

    def get_updated_field(self):
        return 'last_update_timestamp'


class NewsItemIndex(indexes.SearchIndex, indexes.Indexable):
    title = indexes.CharField(model_attr='title', boost=1.5)
    text = indexes.CharField(document=True, use_template=True)
    tags = indexes.MultiValueField(faceted=True)
    date = indexes.DateField(model_attr='date')
    news_categories = indexes.CharField(model_attr='category', null=True, faceted=True)

    slug = indexes.CharField(model_attr='slug', indexed=False)

    def prepare(self, obj):
        data = super().prepare(obj)
        today = datetime.datetime.today()
        if obj.date is None:
            return data
        obj_datetime = datetime.datetime(*obj.date.timetuple()[:3])
        if obj_datetime < today - datetime.timedelta(weeks=15):
            # push older news in later pages
            data['boost'] = 0.8
        elif obj_datetime > today - datetime.timedelta(weeks=3):
            # pull recent news
            data['boost'] = 1.5
        return data

    def get_model(self):
        return NewsItem

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]

    def get_updated_field(self):
        return 'last_update_timestamp'


class SoundFileIndex(indexes.SearchIndex, indexes.Indexable):
    title = indexes.CharField(boost=1.5)
    text = indexes.CharField(document=True, use_template=True)
    tags = indexes.MultiValueField(faceted=True)
    date = indexes.DateField()
    categories = indexes.MultiValueField(faceted=True)
    format = indexes.CharField(model_attr='format', null=True, faceted=True)

    def get_model(self):
        return SoundFile

    def prepare_title(self, obj):
        if obj.title:
            return obj.title
        return obj.episode.title

    def prepare_categories(self, obj):
        return [category.title for category in obj.episode.emission.categories.all()]

    def prepare_date(self, obj):
        return obj.episode.first_diffusion_date.date() if obj.episode.first_diffusion_date else None

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.episode.tags.all()]

    def get_updated_field(self):
        return 'last_update_timestamp'

    def load_all_queryset(self):
        return (
            SoundFile.objects.published().select_related().prefetch_related('episode__emission__categories')
        )

    def index_queryset(self, using=None):
        return self.get_model()._default_manager.published()
