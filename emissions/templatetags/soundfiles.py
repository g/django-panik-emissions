import os
import stat
import urllib.parse

from django.conf import settings
from django.template import Library

register = Library()


@register.filter(name='is_format_available')
def is_available(soundfile, format='ogg'):
    if soundfile is None:
        return False
    sound_path = soundfile.get_format_path(format)
    if not sound_path:
        return False
    if settings.DEBUG:
        return True
    return os.path.exists(sound_path)


@register.filter(name='format_url')
def format_url(soundfile, format='ogg'):
    return soundfile.get_format_url(format)


@register.filter
def format_length(soundfile, format='ogg'):
    if not is_available(soundfile, format):
        return 0
    sound_path = soundfile.get_format_path(format)
    return os.stat(sound_path)[stat.ST_SIZE]


@register.filter
def sound_filename(soundfile):
    if soundfile.file:
        return soundfile.file.name.split('/')[-1]
    if soundfile.external_url:
        return urllib.parse.urlsplit(soundfile.external_url).path.strip('/').split('/')[-1]


@register.filter
def as_duration(value):
    if not value:
        return '-'
    return '%s:%02d' % (value // 60, value % 60)
