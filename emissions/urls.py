from django.urls import path, re_path

from .views import *

urlpatterns = [
    path('', EmissionListView.as_view(), name='emission-list'),
    path('archived', ArchivedEmissionsListView.as_view(), name='emission-archived-list'),
    path('categories', CategoryListView.as_view(), name='category-list'),
    path('days', DaysView.as_view(), name='days'),
    path('add', EmissionCreateView.as_view(), name='emission-add'),
    re_path(r'^upload/(?P<transaction_id>[a-zA-Z0-9-]+)/$', UploadView.as_view(), name='upload'),
    path('news/', NewsListView.as_view(), name='news-list'),
    re_path(r'^news/(?P<slug>[\w,-]+)/$', NewsItemDetailView.as_view(), name='newsitem-view'),
    re_path(r'^news/(?P<slug>[\w,-]+)/delete$', NewsItemDeleteView.as_view(), name='newsitem-delete'),
    re_path(r'^news/(?P<slug>[\w,-]+)/edit$', NewsItemUpdateView.as_view(), name='newsitem-update'),
    path('add-newsitem', NewsItemAddView.as_view(), name='newsitem-add'),
    path('manage/categories/', CategoryManageView.as_view(), name='emission-categories-manage'),
    path('manage/categories/add/', CategoryManageAddView.as_view(), name='emission-categories-manage-add'),
    path(
        'manage/categories/edit/<int:pk>/',
        CategoryManageEditView.as_view(),
        name='emission-categories-manage-edit',
    ),
    path(
        'manage/categories/delete/<int:pk>/',
        CategoryManageDeleteView.as_view(),
        name='emission-categories-manage-delete',
    ),
    path('manage/news-categories/', NewsCategoryManageView.as_view(), name='emission-news-categories-manage'),
    path(
        'manage/news-categories/add/',
        NewsCategoryManageAddView.as_view(),
        name='emission-news-categories-manage-add',
    ),
    path(
        'manage/news-categories/edit/<int:pk>/',
        NewsCategoryManageEditView.as_view(),
        name='emission-news-categories-manage-edit',
    ),
    path(
        'manage/news-categories/delete/<int:pk>/',
        NewsCategoryManageDeleteView.as_view(),
        name='emission-news-categories-manage-delete',
    ),
    path('manage/formats/', FormatManageView.as_view(), name='emission-formats-manage'),
    path('manage/formats/add/', FormatManageAddView.as_view(), name='emission-formats-manage-add'),
    path(
        'manage/formats/edit/<int:pk>/',
        FormatManageEditView.as_view(),
        name='emission-formats-manage-edit',
    ),
    path(
        'manage/formats/delete/<int:pk>/',
        FormatManageDeleteView.as_view(),
        name='emission-formats-manage-delete',
    ),
    re_path(r'^(?P<slug>[\w,-]+)/$', EmissionDetailView.as_view(), name='emission-view'),
    re_path(r'^(?P<slug>[\w,-]+)/edit/$', EmissionUpdateView.as_view(), name='emission-update'),
    re_path(r'^(?P<slug>[\w,-]+)/delete/$', EmissionDeleteView.as_view(), name='emission-delete'),
    re_path(
        r'^(?P<slug>[\w,-]+)/add-schedule$', EmissionAddScheduleView.as_view(), name='emission-add-schedule'
    ),
    re_path(
        r'^(?P<slug>[\w,-]+)/add-absence$', EmissionAddAbsenceView.as_view(), name='emission-add-absence'
    ),
    re_path(r'^(?P<slug>[\w,-]+)/chat/open/$', EmissionOpenChatView.as_view(), name='emission-open-chat'),
    re_path(r'^(?P<slug>[\w,-]+)/chat/close/$', EmissionCloseChatView.as_view(), name='emission-close-chat'),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/schedule/(?P<pk>\d+)/edit$',
        ScheduleUpdateView.as_view(),
        name='schedule-edit',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/schedule/(?P<pk>\d+)/remove$',
        ScheduleDeleteView.as_view(),
        name='schedule-delete',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/absence/(?P<pk>\d+)/remove$',
        AbsenceDeleteView.as_view(),
        name='absence-delete',
    ),
    re_path(r'^(?P<emission_slug>[\w,-]+)/add$', EpisodeCreateView.as_view(), name='episode-add'),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/$', EpisodeDetailView.as_view(), name='episode-view'
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/edit/$',
        EpisodeUpdateView.as_view(),
        name='episode-update',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/delete/$',
        EpisodeDeleteView.as_view(),
        name='episode-delete',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/start/$',
        EpisodeStartView.as_view(),
        name='episode-start',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/stop/$',
        EpisodeStopView.as_view(),
        name='episode-stop',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/add-soundfile$',
        EpisodeAddSoundFileView.as_view(),
        name='episode-add-soundfile',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/add-diffusion$',
        EpisodeAddDiffusionView.as_view(),
        name='episode-add-diffusion',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/diffusion/(?P<pk>\d+)/remove$',
        DiffusionDeleteView.as_view(),
        name='diffusion-delete',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/sounds/(?P<pk>\d+)/remove$',
        SoundFileDeleteView.as_view(),
        name='soundfile-delete',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/sounds/(?P<pk>\d+)/$',
        SoundFileUpdateView.as_view(),
        name='soundfile-update',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/sounds/update-order$',
        EpisodeSoundFilesUpdateOrderView.as_view(),
        name='episode-soundfiles-update-order',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/regie/$',
        EpisodeRegieView.as_view(),
        name='episode-regie',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/regie/marks/$',
        EpisodeRegieMarks.as_view(),
        name='episode-regie-marks',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/regie/delete/$',
        EpisodeRegieDeleteElementView.as_view(),
        name='episode-regie-delete-element',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/regie/update-order$',
        EpisodeRegieUpdateOrderView.as_view(),
        name='episode-regie-update-order',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/add-newsitem$',
        EmissionNewsItemAdd.as_view(),
        name='emission-newsitem-add',
    ),
    re_path(
        r'^category/(?P<slug>[\w,-]+)/add-newsitem$', CategoryNewsItemAddView.as_view(), name='newsitem-add'
    ),
    # images
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/pictures-order/$',
        EpisodePicturesOrderView.as_view(),
        name='episode-pictures-order',
    ),
    re_path(
        r'^(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/add-picture/$',
        EpisodePictureAddView.as_view(),
        name='episode-picture-add',
    ),
    path(
        'manage/images/edit/<int:pk>/',
        PictureEditView.as_view(),
        name='picture-edit',
    ),
    path(
        'manage/images/delete/<int:pk>/',
        PictureDeleteView.as_view(),
        name='picture-delete',
    ),
]
