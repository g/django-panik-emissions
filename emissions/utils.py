import os
import re
import struct
import subprocess
from datetime import datetime, time, timedelta

import piexif
import taggit.utils
from django.db.models import Q
from django.utils.encoding import force_str
from django.utils.module_loading import import_string
from PIL import ExifTags, Image

from .app_settings import app_settings


def get_duration(filename):
    p = subprocess.Popen(
        ['mediainfo', '--Inform=Audio;%Duration%', filename],
        close_fds=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = p.communicate()
    try:
        return int(stdout) / 1000
    except ValueError:
        pass

    # fallback on soxi
    p = subprocess.Popen(
        ['soxi', filename],
        close_fds=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = p.communicate()
    for line in stdout.splitlines():
        line = force_str(line)
        if not line.startswith('Duration'):
            continue
        try:
            hours, minutes, seconds, cs = re.findall(r'(\d\d):(\d\d):(\d\d)\.(\d\d)', line)[0]
        except IndexError:
            continue
        return int(hours) * 3600 + int(minutes) * 60 + int(seconds) + (int(cs) / 100)
    return None


def maybe_resize(image_path):
    if not os.path.exists(image_path):
        return
    image = Image.open(image_path)

    kwargs = {}
    save_image = False

    if 'exif' in image.info:
        try:
            exif_dict = piexif.load(image.info['exif'])
        except struct.error:
            exif_dict = {}
        if exif_dict.get('GPS'):
            exif_dict['GPS'] = {}
            save_image = True
        if 'thumbnail' in exif_dict:
            del exif_dict['thumbnail']
            save_image = True
        for ifd in list(exif_dict.get('Exif', {}).keys()):
            # remove all tags except width/height/orientation
            # this is because some phone/apps create bogus metadata
            # that would break basic things like rotation.
            if ifd not in (
                ExifTags.Base.ExifImageWidth,
                ExifTags.Base.ExifImageHeight,
                ExifTags.Base.Orientation,
            ):
                del exif_dict['Exif'][ifd]
                save_image = True
        if save_image:
            try:
                kwargs = {'exif': piexif.dump(exif_dict)}
            except ValueError:
                # really broken content, remove it all
                kwargs = {'exif': piexif.dump({})}

    if max(image.size) > 1400:
        # no sense storing images that large
        factor = 1400.0 / max(image.size)
        image = image.resize((int(image.size[0] * factor), int(image.size[1] * factor)), Image.LANCZOS)
        if image.mode == 'RGBA':
            # replace alpha channel, use a white background
            white_bg = Image.new('RGBA', image.size, (255, 255, 255, 255))
            image = Image.alpha_composite(white_bg, image)
            image = image.convert('RGB')
        if image_path.endswith('.png'):
            # make sure png files are always RGB
            # (it happened cmyk jpeg files were uploaded with a .png extension and that would fail)
            image = image.convert('RGB')
        save_image = True

    if save_image:
        image.save(image_path, **kwargs)


def whatsonair(now=None, content_filter=None):
    from .models import Diffusion, Nonstop, Schedule

    if now is None:
        now = datetime.now()
    # get program around current time, the deltas allow for very long episodes
    program = period_program(
        now - timedelta(hours=app_settings.MAX_EPISODE_RUNNING_TIME),
        now + timedelta(hours=app_settings.MAX_EPISODE_RUNNING_TIME),
        prefetch_sounds=False,
        prefetch_categories=False,
        include_nonstop=False,
        content_filter=content_filter,
    )
    program = [x for x in program if not x.datetime > now]

    emission = None
    episode = None
    nonstop = None
    current_slot = None
    if program and program[-1].datetime + timedelta(minutes=program[-1].get_duration()) > now:
        current_slot = program[-1]
        if isinstance(current_slot, Schedule):
            emission = current_slot.emission
        elif isinstance(current_slot, Diffusion):
            episode = current_slot.episode
            emission = episode.emission
    else:
        for nonstop in Nonstop.objects.all():
            if (
                nonstop.start < nonstop.end and (now.time() >= nonstop.start and now.time() < nonstop.end)
            ) or (nonstop.start > nonstop.end and (now.time() >= nonstop.start or now.time() < nonstop.end)):
                current_slot = nonstop
                break
        else:
            nonstop = None

    return {'emission': emission, 'episode': episode, 'nonstop': nonstop, 'current_slot': current_slot}


def period_program(
    date_start,
    date_end,
    prefetch_sounds=True,
    prefetch_categories=True,
    include_nonstop=True,
    content_filter=None,
):
    from .models import Absence, Diffusion, Nonstop, Schedule, SoundFile, WeekdayMixin

    dawn = time(app_settings.DAY_HOUR_START, app_settings.DAY_MINUTE_START)

    diffusions = (
        Diffusion.objects.select_related()
        .filter(datetime__range=(date_start, date_end))
        .exclude(info_only_schedule=True)
        .order_by('datetime')
    )
    if prefetch_categories:
        diffusions = diffusions.prefetch_related('episode__emission__categories')
    if content_filter:
        diffusions = import_string(content_filter)()['diffusions_filter'](diffusions)
    diffusions = [x for x in diffusions if x.datetime >= date_start and x.datetime < date_end]

    if prefetch_sounds:
        soundfiles = {}
        for soundfile in (
            SoundFile.objects.published()
            .select_related()
            .filter(fragment=False, episode__in=[x.episode for x in diffusions])
        ):
            soundfiles[soundfile.episode_id] = soundfile

        for diffusion in diffusions:
            diffusion.episode.main_sound = soundfiles.get(diffusion.episode.id)

    # the secondary sortkey puts schedules that happens everyweek after
    # specific ones, this will be useful later on, when multiple schedules
    # happen at the same time and we have to remove the least specific.
    period_schedules = (
        Schedule.objects.filter(Q(start_date__isnull=True) | Q(start_date__lt=date_end))
        .filter(Q(end_date__isnull=True) | Q(end_date__gt=date_start))
        .select_related()
        .order_by('datetime', 'weeks')
    )
    if prefetch_categories:
        period_schedules = period_schedules.prefetch_related('emission__categories')

    if content_filter:
        period_schedules = import_string(content_filter)()['schedules_filter'](period_schedules)

    program = []
    current_date = date_start
    while current_date < date_end:
        week_day = current_date.weekday()
        week_no = (current_date.day - 1) // 7
        day_schedules = [
            x for x in period_schedules if x.get_weekday() == week_day and x.match_week(week_no, current_date)
        ]
        for schedule in day_schedules:
            schedule.datetime = datetime(
                current_date.year,
                current_date.month,
                current_date.day,
                schedule.datetime.hour,
                schedule.datetime.minute,
            )
            schedule.datetime += timedelta(days=schedule.datetime.weekday() - current_date.weekday())
            if week_day == 6 and schedule.datetime.weekday() == 0:
                # on Sundays we can have Sunday->Monday night programming, and
                # we need to get them on the right date.
                schedule.datetime += timedelta(days=7)
            if schedule.datetime.time() < dawn:
                schedule.datetime += timedelta(days=1)
        program.extend(day_schedules)
        current_date += timedelta(days=1)

    program.sort(key=lambda x: x.datetime)

    absences = {}
    for absence in Absence.objects.filter(datetime__range=(date_start, date_end)):
        absences[absence.datetime] = True

    for i, schedule in enumerate(program):
        if schedule is None:
            continue

        # look for a diffusion matching this schedule
        d = [x for x in diffusions if x.datetime.timetuple()[:5] == schedule.datetime.timetuple()[:5]]
        if d:
            diffusions.remove(d[0])
            d[0].rerun = program[i].rerun  # keep track of reruns
            program[i] = d[0]
            for j, other_schedule in enumerate(program[i + 1 :]):
                # remove other emissions scheduled at the same time
                if other_schedule.datetime.timetuple()[:5] == schedule.datetime.timetuple()[:5]:
                    program[i + 1 + j] = None
                else:
                    break

        if schedule.datetime in absences and isinstance(program[i], Schedule):
            program[i] = None
            continue

    # here we are with remaining diffusions, those that were not overriding a
    # planned schedule
    for diffusion in diffusions:
        program = [x for x in program if x is not None]
        try:
            just_before_program = [x for x in program if x.datetime < diffusion.datetime][-1]
            new_diff_index = program.index(just_before_program) + 1
        except IndexError:
            # nothing before
            new_diff_index = 0
        program.insert(new_diff_index, diffusion)

        # cut (or even remove) programs that started earlier but continued over
        # this program start time
        i = new_diff_index
        while i > 0:
            previous = program[i - 1]
            previous_endtime = previous.datetime + timedelta(minutes=previous.get_duration())
            if previous_endtime > diffusion.datetime:
                previous.duration = (diffusion.datetime - previous.datetime).seconds / 60
                if previous.duration <= 0:
                    program[i - 1] = None
            i -= 1

        # push back (or remove) programs that started before this program ends
        # (this may be unnecessary as next step does that again for all
        # programs)
        diffusion_endtime = diffusion.datetime + timedelta(minutes=diffusion.get_duration())
        i = new_diff_index
        while i < len(program) - 1:
            next_prog = program[i + 1]
            if next_prog.datetime < diffusion_endtime:
                diff = diffusion_endtime - next_prog.datetime
                if (diff.seconds / 60) >= next_prog.get_duration():
                    program[i + 1] = None
                else:
                    next_prog.datetime = diffusion_endtime
                    next_prog.duration = next_prog.get_duration() - (diff.seconds / 60)
            i += 1

    # remove overlapping programs
    program = [x for x in program if x is not None]
    for i, slot in enumerate(program):
        if slot is None:
            continue

        slot_end = slot.datetime + timedelta(minutes=slot.get_duration())

        j = i + 1
        while j < len(program) - 1:
            if program[j]:
                if slot_end > program[j].datetime:
                    program[j] = None
            j += 1

    program = [x for x in program if x is not None]

    if not include_nonstop:
        return program

    # last step is adding nonstop zones between slots
    nonstops = list(Nonstop.objects.all().order_by('start'))
    nonstops = [x for x in nonstops if x.start != x.end]
    try:
        first_of_the_day = [x for x in nonstops if x.start <= dawn][-1]
        nonstops = nonstops[nonstops.index(first_of_the_day) :] + nonstops[: nonstops.index(first_of_the_day)]
    except IndexError:
        pass

    class NonstopSlot(WeekdayMixin):
        def __init__(self, nonstop, dt):
            self.datetime = dt
            self.title = nonstop.title
            self.slug = nonstop.slug
            self.label = nonstop.get_public_label()
            self.nonstop = nonstop

        def __repr__(self):
            return '<Nonstop Slot %r>' % self.label

        def get_duration(self):
            # fake duration
            return 0

        @classmethod
        def get_serie(cls, start, end):
            cells = []
            dt = None
            delta_day = 0
            last_added_end = None
            for nonstop in nonstops:
                nonstop_day_start = start.replace(hour=nonstop.start.hour, minute=nonstop.start.minute)
                nonstop_day_end = start.replace(hour=nonstop.end.hour, minute=nonstop.end.minute)
                nonstop_day_start += timedelta(days=delta_day)
                nonstop_day_end += timedelta(days=delta_day)
                if nonstop.start > nonstop.end:
                    nonstop_day_end += timedelta(days=1)

                if nonstop_day_start < end and nonstop_day_end > start:
                    if dt is None:
                        dt = start
                    else:
                        dt = nonstop_day_start
                    cells.append(cls(nonstop, dt))
                    last_added_end = nonstop_day_end
                    if nonstop.start > nonstop.end:
                        # we just added a midnight crossing slot, future slots
                        # should be one day later.
                        delta_day += 1
                    if nonstop.start < dawn and nonstop.end > dawn and start.time() < dawn:
                        dt = dt.replace(hour=dawn.hour, minute=dawn.minute)
                        cells.append(cls(nonstop, dt))

            cells.sort(key=lambda x: x.datetime)
            if last_added_end and last_added_end < end:
                # we used all nonstop slots and we still did not reach the end;
                # let's go for one more turn.
                cells.extend(cls.get_serie(last_added_end, end))
            return cells

    first_day_start = datetime(
        *date_start.replace(
            hour=app_settings.DAY_HOUR_START, minute=app_settings.DAY_MINUTE_START
        ).timetuple()[:5]
    )
    last_day_end = datetime(
        *date_end.replace(hour=app_settings.DAY_HOUR_START, minute=app_settings.DAY_MINUTE_START).timetuple()[
            :5
        ]
    )

    if program:
        program[0:0] = NonstopSlot.get_serie(first_day_start, program[0].datetime)

    i = 0
    while i < len(program) - 1:
        slot = program[i]
        if not isinstance(slot, NonstopSlot):
            slot_end = slot.datetime + timedelta(minutes=slot.get_duration())
            next_slot = program[i + 1]

            if slot_end < next_slot.datetime:
                next_day_start = next_slot.datetime.replace(
                    hour=app_settings.DAY_HOUR_START, minute=app_settings.DAY_MINUTE_START
                )
                if slot_end < next_day_start and next_slot.datetime > next_day_start:
                    nonstop_day_slots = NonstopSlot.get_serie(slot_end, next_day_start)
                    nonstop_next_day_slots = NonstopSlot.get_serie(next_day_start, next_slot.datetime)
                    if (
                        nonstop_day_slots
                        and nonstop_next_day_slots
                        and nonstop_day_slots[-1].label == nonstop_next_day_slots[0].label
                    ):
                        nonstop_next_day_slots = nonstop_next_day_slots[1:]
                    program[i + 1 : i + 1] = nonstop_day_slots + nonstop_next_day_slots
                else:
                    program[i + 1 : i + 1] = NonstopSlot.get_serie(slot_end, next_slot.datetime)

        i += 1

    if program:
        program.extend(
            NonstopSlot.get_serie(
                program[-1].datetime + timedelta(minutes=program[-1].get_duration()), last_day_end
            )
        )

    # add end_datetime to nonstop slots
    for i, slot in enumerate(program):
        if isinstance(slot, NonstopSlot):
            if i < len(program) - 1:
                slot.end_datetime = program[i + 1].datetime
            else:
                slot.end_datetime = date_end.replace(
                    hour=app_settings.DAY_HOUR_START, minute=app_settings.DAY_MINUTE_START
                )

    return program


def day_program(date, prefetch_sounds=True, prefetch_categories=True, include_nonstop=True):
    date_start = datetime(
        *date.timetuple()[:3], hour=app_settings.DAY_HOUR_START, minute=app_settings.DAY_MINUTE_START
    )
    date_end = date_start + timedelta(days=1)
    return period_program(
        date_start,
        date_end,
        prefetch_sounds=prefetch_sounds,
        prefetch_categories=prefetch_categories,
        include_nonstop=include_nonstop,
    )


def custom_parse_tags(tagstring):
    return [x.lower() for x in taggit.utils._parse_tags(tagstring)]
