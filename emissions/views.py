import datetime
import io
import json
import logging
import os
import time

import haystack.views
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import RedirectView, TemplateView, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from sorl.thumbnail.shortcuts import get_thumbnail

from .app_settings import app_settings
from .forms import (
    AbsenceForm,
    DiffusionForm,
    EmissionForm,
    EpisodeForm,
    EpisodeNewForm,
    NewsItemForm,
    PictureForm,
    PlaylistElementForm,
    ScheduleForm,
    SoundFileEditForm,
    SoundFileForm,
)
from .models import (
    Absence,
    Category,
    Diffusion,
    Emission,
    Episode,
    Format,
    NewsCategory,
    NewsItem,
    Picture,
    PlaylistElement,
    Schedule,
    SoundFile,
)

__all__ = [
    'ArchivedEmissionsListView',
    'EmissionListView',
    'EmissionDetailView',
    'EmissionCreateView',
    'EmissionUpdateView',
    'EmissionDeleteView',
    'EpisodeCreateView',
    'EpisodeDetailView',
    'EpisodeUpdateView',
    'EpisodeDeleteView',
    'EmissionAddScheduleView',
    'ScheduleDeleteView',
    'ScheduleUpdateView',
    'CategoryListView',
    'DaysView',
    'UploadView',
    'EpisodeAddSoundFileView',
    'EpisodeAddDiffusionView',
    'DiffusionDeleteView',
    'EmissionNewsItemAdd',
    'NewsListView',
    'NewsItemDetailView',
    'NewsItemUpdateView',
    'CategoryNewsItemAddView',
    'NewsItemDeleteView',
    'NewsItemAddView',
    'SoundFileDeleteView',
    'SoundFileUpdateView',
    'EpisodeSoundFilesUpdateOrderView',
    'EmissionAddAbsenceView',
    'AbsenceDeleteView',
    'EmissionOpenChatView',
    'EmissionCloseChatView',
    'EpisodeRegieView',
    'EpisodeRegieUpdateOrderView',
    'EpisodeRegieMarks',
    'EpisodeRegieDeleteElementView',
    'EpisodeStartView',
    'EpisodeStopView',
    'CategoryManageView',
    'CategoryManageAddView',
    'CategoryManageEditView',
    'CategoryManageDeleteView',
    'FormatManageView',
    'FormatManageAddView',
    'FormatManageEditView',
    'FormatManageDeleteView',
    'NewsCategoryManageView',
    'NewsCategoryManageAddView',
    'NewsCategoryManageEditView',
    'NewsCategoryManageDeleteView',
    'EpisodePicturesOrderView',
    'EpisodePictureAddView',
    'PictureEditView',
    'PictureDeleteView',
]

SUCCESS_MESSAGE = gettext_lazy('Your changes will appear online in a few minutes.')

logger = logging.getLogger('panikdb')


class EmissionListView(ListView):
    model = Emission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_type'] = 'alphabetical'
        return context

    def get_queryset(self):
        return Emission.objects.exclude(archived=True).prefetch_related('schedule_set').order_by('title')


class ArchivedEmissionsListView(ListView):
    model = Emission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_type'] = 'archived'
        return context

    def get_queryset(self):
        return Emission.objects.filter(archived=True).order_by('title')


class CategoryListView(ListView):
    model = Category

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_type'] = 'categories'
        return context

    def get_queryset(self):
        return Category.objects.order_by('title')


class EmissionDetailView(DetailView):
    model = Emission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['has_emission_chats'] = getattr(settings, 'HAS_EMISSION_CHATS', False)
        schedules = (
            Schedule.objects.select_related()
            .filter(emission=self.object)
            .order_by('datetime', 'start_date', 'end_date')
        )
        context['schedules'] = [x for x in schedules if x.is_current()]
        context['past_schedules'] = [x for x in schedules if x.is_past()]
        context['future_schedules'] = [x for x in schedules if x.is_future()]
        context['schedules_without_reruns'] = [x for x in context['schedules'] if not x.rerun]

        # get all episodes, with an additional attribute to get the date of
        # their first diffusion
        context['episodes'] = self.object.get_sorted_episodes()

        context['newsitems'] = self.object.get_sorted_newsitems()
        context['absences'] = (
            self.object.absence_set.select_related()
            .filter(datetime__gte=datetime.date.today())
            .order_by('datetime')
        )

        if not 'all' in self.request.GET:
            context['episodes'] = context['episodes'][:10]
            if context['absences']:
                context['newsitems'] = context['newsitems'][:5]
            else:
                context['newsitems'] = context['newsitems'][:10]

        try:
            context['can_manage'] = self.request.user.can_manage(self.object)
        except AttributeError:
            pass

        return context


class EmissionCreateView(CreateView):
    form_class = EmissionForm
    model = Emission

    success_url = reverse_lazy('emission-list')

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('emissions.add_emission'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return super().get_success_url()


class EmissionUpdateView(UpdateView):
    form_class = EmissionForm
    model = Emission

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return super().get_success_url()


class EmissionDeleteView(DeleteView):
    model = Emission
    success_url = reverse_lazy('home')

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('emissions.delete_emission'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return super().get_success_url()


class EpisodeCreateView(CreateView):
    model = Episode
    form_class = EpisodeNewForm

    def get_form(self, *args, **kwargs):
        emission = Emission.objects.get(slug=self.kwargs.get('emission_slug'))
        if not self.request.user.can_manage(emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_initial(self):
        initial = super().get_initial()
        initial['emission'] = Emission.objects.get(slug=self.kwargs.get('emission_slug'))
        initial['duration'] = initial['emission'].get_next_planned_duration()
        since = datetime.datetime.today()
        while True:
            initial['first_diffusion'] = initial['emission'].get_next_planned_date(since=since)
            if not Diffusion.objects.filter(datetime=initial['first_diffusion']).exists():
                break
            since = initial['first_diffusion'] + datetime.timedelta(hours=1)
        for i, schedule in enumerate(
            Schedule.objects.filter(emission=initial['emission'], rerun=True).order_by('datetime')
        ):
            rerun_date = schedule.get_next_planned_date(initial['first_diffusion'])
            if i == 0:
                initial['second_diffusion'] = rerun_date
            elif i == 1:
                initial['third_diffusion'] = rerun_date
            elif i == 2:
                initial['fourth_diffusion'] = rerun_date
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['emission'] = Emission.objects.get(slug=self.kwargs.get('emission_slug'))
        return context

    def get_success_url(self):
        logger.info('added episode for %s (%s)', self.object.emission, self.object)
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.get_absolute_url()


class EmissionEpisodeMixin:
    def get_queryset(self):
        return self.model.objects.filter(emission__slug=self.kwargs['emission_slug'])


class EpisodeDetailView(EmissionEpisodeMixin, DetailView):
    model = Episode

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['diffusions'] = Diffusion.objects.filter(episode=self.object.id)
        context['soundfiles'] = SoundFile.objects.filter(episode=self.object.id)

        now = datetime.datetime.now()
        context['has_future_diffusion'] = any(x for x in context['diffusions'] if x.datetime > now)

        try:
            context['can_manage'] = self.request.user.can_manage(self.object.emission)
        except AttributeError:
            pass

        return context


class EpisodeUpdateView(EmissionEpisodeMixin, UpdateView):
    form_class = EpisodeForm
    model = Episode

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object.emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return super().get_success_url()


class EpisodeDeleteView(EmissionEpisodeMixin, DeleteView):
    model = Episode

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object.emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        logger.info('deleted episode for %s (%s)', self.object.emission, self.object)
        return reverse('emission-view', kwargs={'slug': self.object.emission.slug})


class EmissionAddScheduleView(CreateView):
    form_class = ScheduleForm
    model = Schedule

    def get_initial(self):
        initial = super().get_initial()
        initial['emission'] = Emission.objects.get(slug=self.kwargs.get('slug'))
        return initial

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('emissions.change_schedule'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        logger.info('added schedule %s', self.object)
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.emission.get_absolute_url()


class ScheduleUpdateView(UpdateView):
    form_class = ScheduleForm
    model = Schedule

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('emissions.change_schedule'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        logger.info('modified schedule %s', self.object)
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.emission.get_absolute_url()


class ScheduleDeleteView(RedirectView):
    def get_redirect_url(self, emission_slug, pk):
        if not self.request.user.has_perm('emissions.change_schedule'):
            raise PermissionDenied()
        schedule = Schedule.objects.get(id=pk)
        logger.info('deleted schedule %s', schedule)
        Schedule.objects.filter(id=pk).delete()
        messages.success(self.request, SUCCESS_MESSAGE)
        return reverse('emission-view', kwargs={'slug': str(emission_slug)})


class EmissionAddAbsenceView(CreateView):
    form_class = AbsenceForm
    model = Absence

    def get_initial(self):
        initial = super().get_initial()
        initial['emission'] = Emission.objects.get(slug=self.kwargs.get('slug'))
        initial['user'] = self.request.user
        return initial

    def get_success_url(self):
        logger.info('added absence for %s (%s)', self.object.emission, self.object.datetime)
        messages.success(self.request, SUCCESS_MESSAGE)
        notification_emails = [
            x for x in app_settings.ABSENCE_NOTIFICATION_EMAILS if x != self.request.user.email
        ]
        if notification_emails:
            ctx = {
                'emission': self.object.emission,
                'datetime': self.object.datetime,
                'url': self.request.build_absolute_uri(self.object.emission.get_absolute_url()),
            }
            body = render_to_string('emissions/emails/new_absence_body.txt', ctx)
            html_body = render_to_string('emissions/emails/new_absence_body.html', ctx)
            send_mail(
                from_email=None,
                subject=_('Absence declared for %s') % self.object.emission,
                message=body,
                html_message=html_body,
                recipient_list=app_settings.ABSENCE_NOTIFICATION_EMAILS,
                fail_silently=True,
            )
        return self.object.emission.get_absolute_url()

    def get_form(self, *args, **kwargs):
        emission = Emission.objects.get(slug=self.kwargs.get('slug'))
        if not self.request.user.can_manage(emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)


class AbsenceDeleteView(RedirectView):
    def get_redirect_url(self, emission_slug, pk):
        emission = Emission.objects.get(slug=emission_slug)
        if not self.request.user.can_manage(emission):
            raise PermissionDenied()
        absence = Absence.objects.get(id=pk)
        logger.info('deleted absence for %s (%s)', absence.emission, absence.datetime)
        Absence.objects.filter(id=pk).delete()
        messages.success(self.request, SUCCESS_MESSAGE)
        return reverse('emission-view', kwargs={'slug': str(emission_slug)})


class DaysView(TemplateView):
    template_name = 'emissions/days.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_type'] = 'days'
        schedules = Schedule.objects.all().select_related().order_by('datetime')
        days = []
        for day in range(7):
            # do not duplicate emissions with multiple schedules on same day
            day_schedules = {
                (x.datetime.time(), x.emission_id): x for x in schedules if x.is_on_weekday(day + 1)
            }
            days.append(
                {
                    'schedules': sorted(day_schedules.values(), key=lambda x: x.datetime),
                    'datetime': datetime.datetime(2007, 1, day + 1),
                }
            )
        context['days'] = days
        return context


class JSONResponse(HttpResponse):
    """JSON response class."""

    def __init__(self, obj='', json_opts={}, mimetype='application/json', *args, **kwargs):
        content = json.dumps(obj, **json_opts)
        super().__init__(content, mimetype, *args, **kwargs)


class UploadView(View):
    def response_mimetype(self, request):
        if 'application/json' in request.headers['accept']:
            return 'application/json'
        else:
            return 'text/plain'

    @csrf_exempt
    def post(self, request, transaction_id):
        max_filename_length = 256
        url = reverse('upload', kwargs={'transaction_id': transaction_id})
        if request.FILES is None:
            response = JSONResponse({}, {}, self.response_mimetype(request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        data = []
        for uploaded_file in request.FILES.values():
            path = os.path.join('upload', str(transaction_id), uploaded_file.name)
            filename = default_storage.save(path, uploaded_file)
            url = '%s%s' % (url, os.path.basename(filename))
            data.append({'name': uploaded_file.name, 'size': uploaded_file.size, 'url': url})
        response = JSONResponse(data, {}, self.response_mimetype(request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class EpisodeAddSoundFileView(CreateView):
    form_class = SoundFileForm
    model = SoundFile

    def get_initial(self):
        context = {
            'episode': Episode.objects.get(
                slug=self.kwargs.get('slug'), emission__slug=self.kwargs.get('emission_slug')
            ),
            'title': _('Record'),
        }
        context['license'] = context['episode'].emission.default_license
        return context

    def get_form(self, *args, **kwargs):
        emission = Emission.objects.get(slug=self.kwargs.get('emission_slug'))
        if not self.request.user.can_manage(emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        if not form.instance.duration:
            form.instance.compute_duration()
            now = datetime.datetime.now()
            if not form.instance.fragment:
                diffusions = Diffusion.objects.filter(episode=form.instance.episode_id)
                if any(x for x in diffusions if x.datetime > now):
                    if form.instance.is_too_short():
                        messages.warning(
                            self.request,
                            _('The sound file duration does not match the expected duration (too short).'),
                        )
                    if form.instance.is_too_long():
                        messages.warning(
                            self.request,
                            _('The sound file duration does not match the expected duration (too long).'),
                        )
            form.instance.save()
        logger.info(
            'added soundfile for %s (%s) (%s)',
            self.object.episode,
            self.object.episode.emission,
            (self.object.title or self.object.id),
        )
        return response

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.episode.get_absolute_url()


class EpisodeAddDiffusionView(CreateView):
    form_class = DiffusionForm
    model = Diffusion

    def get_initial(self):
        return {
            'episode': Episode.objects.get(
                slug=self.kwargs.get('slug'), emission__slug=self.kwargs.get('emission_slug')
            ),
        }

    def get_form(self, *args, **kwargs):
        episode = Episode.objects.get(
            slug=self.kwargs.get('slug'), emission__slug=self.kwargs.get('emission_slug')
        )
        if not self.request.user.can_manage(episode):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_success_url(self):
        logger.info('added diffusion for %s (%s)', self.object.episode.emission, self.object.datetime)
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.episode.get_absolute_url()


class DiffusionDeleteView(RedirectView):
    def get_redirect_url(self, emission_slug, slug, pk):
        episode = Episode.objects.get(slug=slug, emission__slug=emission_slug)
        if not self.request.user.can_manage(episode):
            raise PermissionDenied()
        try:
            diffusion = Diffusion.objects.get(id=pk)
        except Diffusion.DoesNotExist:
            pass
        else:
            logger.info('deleted diffusion for %s (%s)', diffusion.episode.emission, diffusion.datetime)
            Diffusion.objects.filter(id=pk).delete()
            messages.success(self.request, SUCCESS_MESSAGE)
        return reverse('episode-view', kwargs={'emission_slug': str(emission_slug), 'slug': str(slug)})


class FacetedSearchView(haystack.views.FacetedSearchView):
    def extra_context(self):
        context = super().extra_context()
        context['selected_categories'] = [
            x.split(':', 1)[1]
            for x in self.request.GET.getlist('selected_facets')
            if x.startswith('categories_exact')
        ]
        context['selected_tags'] = [
            x.split(':', 1)[1]
            for x in self.request.GET.getlist('selected_facets')
            if x.startswith('tags_exact')
        ]
        return context


class EmissionNewsItemAdd(CreateView):
    model = NewsItem
    form_class = NewsItemForm

    @property
    def emission(self):
        return Emission.objects.get(slug=self.kwargs.get('emission_slug'))

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['emission'] = self.emission
        initial['date'] = datetime.datetime.today()
        initial['user'] = self.request.user
        if NewsCategory.objects.all().count() == 1:
            initial['category'] = NewsCategory.objects.all()[0]
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['emission'] = self.emission
        return context

    def get_success_url(self):
        logger.info('added newsitem for %s (%s)', self.emission, self.object)
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.get_absolute_url()


class NewsItemDetailView(DetailView):
    model = NewsItem

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['can_manage'] = self.request.user.can_manage(self.object)
        return context


class NewsItemUpdateView(UpdateView):
    form_class = NewsItemForm
    model = NewsItem

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['user'] = self.request.user
        return initial

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return super().get_success_url()


class CategoryNewsItemAddView(CreateView):
    model = NewsItem
    form_class = NewsItemForm

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('emissions.add_newsitem'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['category'] = NewsCategory.objects.get(slug=self.kwargs.get('slug'))
        initial['date'] = datetime.datetime.today()
        initial['user'] = self.request.user
        return initial

    def get_success_url(self):
        logger.info('added newsitem (%s)', self.object)
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.get_absolute_url()


class NewsListView(ListView):
    model = NewsCategory

    def get_queryset(self):
        return NewsCategory.objects.order_by('title')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['unsorted_newsitems'] = NewsItem.objects.filter(category__isnull=True).order_by('title')
        return context


class NewsItemAddView(CreateView):
    model = NewsItem
    form_class = NewsItemForm

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('emissions.add_newsitem'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['date'] = datetime.datetime.today()
        initial['user'] = self.request.user
        return initial

    def get_success_url(self):
        logger.info('added newsitem (%s)', self.object)
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.get_absolute_url()


class NewsItemDeleteView(DeleteView):
    model = NewsItem
    success_url = reverse_lazy('home')

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        logger.info('deleted newsitem (%s)', self.object)
        messages.success(self.request, SUCCESS_MESSAGE)
        return super().get_success_url()


class SoundFileDeleteView(DeleteView):
    model = SoundFile

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        logger.info(
            'deleted soundfile for %s (%s) (%s)',
            self.object.episode,
            self.object.episode.emission,
            (self.object.title or self.object.id),
        )
        return reverse(
            'episode-view',
            kwargs={'emission_slug': self.object.episode.emission.slug, 'slug': self.object.episode.slug},
        )


class SoundFileUpdateView(UpdateView):
    template_name_suffix = '_form_update'
    form_class = SoundFileEditForm
    model = SoundFile

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object.episode.emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return '../../'


class EmissionOpenChatView(DetailView):
    model = Emission

    def render_to_response(self, context):
        self.object.chat_open = datetime.datetime.now()
        self.object.save()
        return redirect(self.object.get_absolute_url())


class EmissionCloseChatView(DetailView):
    model = Emission

    def render_to_response(self, context):
        self.object.chat_open = None
        self.object.save()
        return redirect(self.object.get_absolute_url())


class EpisodeRegieView(EmissionEpisodeMixin, DetailView):
    model = Episode
    template_name = 'emissions/episode_regie.html'

    def get_object(self, queryset=None):
        try:
            return Episode.objects.get(
                slug=self.kwargs.get('slug'), emission__slug=self.kwargs.get('emission_slug')
            )
        except Episode.DoesNotExist:
            raise Http404()

    def post(self, request, *args, **kwargs):
        episode = self.get_object()
        form = PlaylistElementForm(request.POST, request.FILES)
        if not form.is_valid():
            messages.error(request, _('Error adding file'))
            return HttpResponseRedirect('.')
        i = PlaylistElement.objects.filter(episode=episode).count()
        playlist_element = PlaylistElement(
            episode=episode,
            title=os.path.basename(form.cleaned_data['sound'].name),
            sound=form.cleaned_data['sound'],
            order=i + 1,
        )
        playlist_element.save()
        messages.info(request, _('File uploaded successfully.'))
        return HttpResponseRedirect('.')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['playlist'] = PlaylistElement.objects.filter(episode=self.object.id)
        context['start_time'] = ''
        context['end_time'] = ''
        if self.object.effective_start:
            context['ready'] = 'ready'
            context['start_time'] = int(
                time.mktime(self.object.effective_start.timetuple()) * 1000
                + self.object.effective_start.microsecond / 1000
            )
        if self.object.effective_end:
            context['end_time'] = int(
                time.mktime(self.object.effective_end.timetuple()) * 1000
                + self.object.effective_end.microsecond / 1000
            )
            context['download_url'] = self.object.get_pige_download_url()

        try:
            context['can_manage'] = self.request.user.can_manage(self.object)
        except AttributeError:
            pass

        context['upload_file_form'] = PlaylistElementForm(initial={'episode': self.object})

        return context


class EpisodeSoundFilesUpdateOrderView(View):
    def get(self, request, *args, **kwargs):
        new_order = request.GET.getlist('new-order[]')
        for element in SoundFile.objects.filter(id__in=new_order):
            element.order = new_order.index(str(element.id)) + 1
            element.save()
        return HttpResponse('ok')


class EpisodeRegieUpdateOrderView(View):
    def get(self, request, *args, **kwargs):
        new_order = request.GET.getlist('new-order[]')
        for element in PlaylistElement.objects.filter(id__in=new_order):
            element.order = new_order.index(str(element.id)) + 1
            element.save()
        return HttpResponse('ok')


class EpisodeRegieDeleteElementView(EpisodeRegieView):
    def get(self, request, *args, **kwargs):
        element_id = request.GET.get('id')
        element = PlaylistElement.objects.get(id=element_id)
        if not self.request.user.can_manage(element.episode):
            return JSONResponse({'err': 1})
        element.delete()
        return HttpResponseRedirect('..')


class EpisodeRegieMarks(EpisodeRegieView):
    def get(self, request, *args, **kwargs):
        episode = self.get_object()
        start = self.request.GET['start']
        end = self.request.GET['end']
        if start:
            episode.effective_start = datetime.datetime.fromtimestamp(float(start) / 1000)
        else:
            episode.effective_start = None
        if end:
            episode.effective_end = datetime.datetime.fromtimestamp(float(end) / 1000)
        else:
            episode.effective_end = None
        episode.save()
        obj = {'err': 0}
        if end:
            obj['pige_download_url'] = episode.get_pige_download_url()
        return JSONResponse(obj)


class EpisodeStartView(EmissionEpisodeMixin, RedirectView):
    def get_redirect_url(self, emission_slug, slug):
        episode = Episode.objects.get(slug=slug, emission__slug=emission_slug)
        episode.effective_start = datetime.datetime.now()
        episode.save()
        messages.success(self.request, _('Started recording'))
        return reverse('episode-view', kwargs={'emission_slug': emission_slug, 'slug': slug})


class EpisodeStopView(EmissionEpisodeMixin, RedirectView):
    def get_redirect_url(self, emission_slug, slug):
        episode = Episode.objects.get(slug=slug, emission__slug=emission_slug)
        episode.effective_end = datetime.datetime.now()
        episode.save()
        messages.success(self.request, _('Stopped recording'))
        return reverse('episode-view', kwargs={'emission_slug': emission_slug, 'slug': slug})


class SuperuserMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class CategoryManageView(SuperuserMixin, ListView):
    model = Category
    template_name = 'emissions/manage/categories_list.html'

    def get_queryset(self):
        return Category.objects.order_by('title')


class CategoryManageAddView(SuperuserMixin, CreateView):
    model = Category
    template_name = 'emissions/manage/category_form.html'
    success_url = reverse_lazy('emission-categories-manage')
    fields = ['title', 'slug', 'itunes_category']


class CategoryManageEditView(SuperuserMixin, UpdateView):
    model = Category
    template_name = 'emissions/manage/category_form.html'
    success_url = reverse_lazy('emission-categories-manage')
    fields = ['title', 'slug', 'itunes_category', 'archived']


class CategoryManageDeleteView(SuperuserMixin, DeleteView):
    model = Category
    template_name = 'emissions/manage/category_confirm_delete.html'
    success_url = reverse_lazy('emission-categories-manage')


class FormatManageView(SuperuserMixin, ListView):
    model = Category
    template_name = 'emissions/manage/formats_list.html'

    def get_queryset(self):
        return Format.objects.order_by('title')


class FormatManageAddView(SuperuserMixin, CreateView):
    model = Format
    template_name = 'emissions/manage/format_form.html'
    success_url = reverse_lazy('emission-formats-manage')
    fields = ['title', 'slug']


class FormatManageEditView(SuperuserMixin, UpdateView):
    model = Format
    template_name = 'emissions/manage/format_form.html'
    success_url = reverse_lazy('emission-formats-manage')
    fields = ['title', 'slug', 'archived']


class FormatManageDeleteView(SuperuserMixin, DeleteView):
    model = Format
    template_name = 'emissions/manage/format_confirm_delete.html'
    success_url = reverse_lazy('emission-formats-manage')


class NewsCategoryManageView(SuperuserMixin, ListView):
    model = NewsCategory
    template_name = 'emissions/manage/news_categories_list.html'

    def get_queryset(self):
        return NewsCategory.objects.order_by('title')


class NewsCategoryManageAddView(SuperuserMixin, CreateView):
    model = NewsCategory
    template_name = 'emissions/manage/category_form.html'
    success_url = reverse_lazy('emission-news-categories-manage')
    fields = ['title', 'slug']


class NewsCategoryManageEditView(SuperuserMixin, UpdateView):
    model = NewsCategory
    template_name = 'emissions/manage/category_form.html'
    success_url = reverse_lazy('emission-news-categories-manage')
    fields = ['title', 'slug', 'archived']


class NewsCategoryManageDeleteView(SuperuserMixin, DeleteView):
    model = NewsCategory
    template_name = 'emissions/manage/category_confirm_delete.html'
    success_url = reverse_lazy('emission-news-categories-manage')


class EpisodePicturesOrderView(View):
    def get(self, request, *args, **kwargs):
        new_order = request.GET.getlist('new-order[]')
        for element in Picture.objects.filter(id__in=new_order):
            element.order = new_order.index(str(element.id)) + 1
            element.save()
        return HttpResponse('ok')


class EpisodePictureAddView(CreateView):
    model = Picture
    form_class = PictureForm

    def get_initial(self):
        context = {
            'episode': Episode.objects.get(
                slug=self.kwargs.get('slug'), emission__slug=self.kwargs.get('emission_slug')
            ),
        }
        return context

    def get_form(self, *args, **kwargs):
        emission = Emission.objects.get(slug=self.kwargs.get('emission_slug'))
        if not self.request.user.can_manage(emission):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        if default_storage.size(self.object.image.name) > 500_000 and not self.object.image.name.endswith(
            '.svg'
        ):
            thumbnail = get_thumbnail(self.object.image.name, '2000x2000', upscale=False, quality=90)
            target_name = os.path.join(
                os.path.dirname(self.object.image.name), 'resized-' + os.path.basename(self.object.image.name)
            )
            self.object.image.name = default_storage.save(target_name, io.BytesIO(thumbnail.read()))
            self.object.save()
        return response

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.episode.get_absolute_url()


class PictureEditView(UpdateView):
    model = Picture
    fields = ('image_attribution_url', 'image_attribution_text', 'title', 'alt_text')

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.get_content_object().get_absolute_url() + '#pictures'


class PictureDeleteView(DeleteView):
    model = Picture

    def get_form(self, *args, **kwargs):
        if not self.request.user.can_manage(self.object):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, SUCCESS_MESSAGE)
        return self.object.get_content_object().get_absolute_url()
