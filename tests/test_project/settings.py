SECRET_KEY = 'django-insecure-test-key'
DEBUG = True
ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'combo.apps.export_import',
    'combo.data',
    'taggit',
    'emissions',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

TIME_ZONE = 'UTC'
USE_TZ = True

STATIC_URL = ''
