import datetime

import pytest

from emissions.models import Emission, Schedule

pytestmark = pytest.mark.django_db


def test_get_next_planned_date():
    emission = Emission.objects.create(slug='test', title='Test', duration=120)
    schedule = Schedule.objects.create(
        emission=emission, datetime=datetime.datetime(2007, 1, 1, 14, 0), weeks=0b1111
    )

    def next_three_dates():
        dt = datetime.datetime(2024, 12, 7, 0, 0)
        r = []
        for i in range(3):
            dt = emission.get_next_planned_date(dt + datetime.timedelta(minutes=10))
            r.append(dt.strftime('%Y-%m-%d %H:%M'))
        return r

    assert next_three_dates() == ['2024-12-09 14:00', '2024-12-16 14:00', '2024-12-23 14:00']

    schedule.datetime = datetime.datetime(2007, 1, 1, 23, 0)
    schedule.save()
    assert next_three_dates() == ['2024-12-09 23:00', '2024-12-16 23:00', '2024-12-23 23:00']

    # schedule past midnight
    schedule.datetime = datetime.datetime(2007, 1, 1, 1, 0)
    schedule.save()
    assert next_three_dates() == ['2024-12-09 01:00', '2024-12-16 01:00', '2024-12-23 01:00']

    # back to 23:00
    schedule.datetime = datetime.datetime(2007, 1, 1, 23, 0)
    schedule.save()

    # 1st Monday
    schedule.weeks = 0b0001
    schedule.save()
    assert next_three_dates() == ['2025-01-06 23:00', '2025-02-03 23:00', '2025-03-03 23:00']

    # 1st & 3rd Monday
    schedule.weeks = 0b0101
    schedule.save()
    assert next_three_dates() == ['2024-12-16 23:00', '2025-01-06 23:00', '2025-01-20 23:00']

    # every other Monday
    schedule.weeks = 0b10000
    schedule.save()
    assert next_three_dates() == ['2024-12-09 23:00', '2024-12-23 23:00', '2025-01-06 23:00']

    # every other Monday (other)
    schedule.weeks = 0b10001
    schedule.save()
    assert next_three_dates() == ['2024-12-16 23:00', '2024-12-30 23:00', '2025-01-13 23:00']
